class Zh < Formula 
  desc "zh is a command line tool for retrieving designs from zeroheight" 
  homepage "www.zeroheight.com" 
  url "https://gitlab.com/zeroheight/zh/repository/archive.tar.gz?ref=1.5.4" 
  version "1.5.4" 
  sha256 "735416ad4b3cfdfc3462a15f2da8cb1e197d588fe9006a2219ed84f24b84495c" 
  depends_on "imagemagick" 
  depends_on "pngcrush" 
 
  def install 
    lib.install Dir["lib/*"] 
    prefix.install "zh" 
    bin.install_symlink prefix/"zh" 
  end 
end 
