class Zh < Formula 
  desc "zh is a command line tool for retrieving design files using zeroheight" 
  homepage "www.zeroheight.com" 
  url "https://bitbucket.org/zeroheight/zh/get/1.0.3.tar.gz" 
  version "1.0.3" 
  sha256 "e118f8ef8ff357c5113c6fd13ebe75239878fd16128de500b308753603c1f884" 
  depends_on "imagemagick" 
 
  def install 
    lib.install Dir["lib/*"] 
    prefix.install "zh" 
    bin.install_symlink prefix/"zh" 
  end 
end 
