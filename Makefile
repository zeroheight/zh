release:
	[ "$(ver)" != "" ] # version must not be null
	git tag $(ver)
	git push --tags
	sleep 2
	wget https://bitbucket.org/zeroheight/zh/get/$(ver).tar.gz
	hash=`openssl dgst -sha256 $(ver).tar.gz` ;\
	read -a hasharr <<< $$hash ;\
	hash=$${hasharr[1]} ;\
	echo 'class Zh < Formula ' > zh.rb ;\
	echo '  desc "zh is a command line tool for retrieving design files using zeroheight" ' >> zh.rb;\
	echo '  homepage "www.zeroheight.com" ' >> zh.rb;\
	echo '  url "https://bitbucket.org/zeroheight/zh/get/$(ver).tar.gz" ' >> zh.rb;\
	echo '  version "$(ver)" ' >> zh.rb;\
	echo "  sha256 \"$$hash\" " >> zh.rb
	echo '  depends_on "imagemagick" ' >> zh.rb
	echo ' ' >> zh.rb
	echo '  def install ' >> zh.rb
	echo '    lib.install Dir["lib/*"] ' >> zh.rb
	echo '    prefix.install "zh" ' >> zh.rb
	echo '    bin.install_symlink prefix/"zh" ' >> zh.rb
	echo '  end ' >> zh.rb
	echo 'end ' >> zh.rb
	rm *.tar.gz*
	git add zh.rb
	git commit -am "creating release formula for $(ver)"
	git push
	
