release:
	[ "$(ver)" != "" ] # version must not be null
	git tag $(ver)
	git push --tags
	sleep 2
	wget https://gitlab.com/zeroheight/zh/repository/archive.tar.gz?ref=$(ver)
	hash=`openssl dgst -sha256 archive.tar.gz?ref=$(ver)` ;\
	read -a hasharr <<< $$hash ;\
	hash=$${hasharr[1]} ;\
	echo 'class Zh < Formula ' > zh.rb ;\
	echo '  desc "zh is a command line tool for retrieving designs from zeroheight" ' >> zh.rb;\
	echo '  homepage "www.zeroheight.com" ' >> zh.rb;\
	echo '  url "https://gitlab.com/zeroheight/zh/repository/archive.tar.gz?ref=$(ver)" ' >> zh.rb;\
	echo '  version "$(ver)" ' >> zh.rb;\
	echo "  sha256 \"$$hash\" " >> zh.rb
	echo '  depends_on "imagemagick" ' >> zh.rb
	echo '  depends_on "pngcrush" ' >> zh.rb
	echo ' ' >> zh.rb
	echo '  def install ' >> zh.rb
	echo '    lib.install Dir["lib/*"] ' >> zh.rb
	echo '    prefix.install "zh" ' >> zh.rb
	echo '    bin.install_symlink prefix/"zh" ' >> zh.rb
	echo '  end ' >> zh.rb
	echo 'end ' >> zh.rb
	rm *.tar.gz*
	git add zh.rb
	git commit -am "creating release formula for $(ver)"
	git push
	
