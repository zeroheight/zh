#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, json
import sys, utils, urllib, apitoken as API_TOKEN

def get_project_id():
    return utils.get_json_file_kv(utils.CONFIG_NAME, "project_id")

def get_last_pull_time():
    return utils.get_json_file_kv(utils.CONFIG_NAME, "last_pull_time")

def set_last_pull_time(time):
    return utils.set_json_file_kv(utils.CONFIG_NAME, "last_pull_time",time)

def set_project_id(uid):
    return utils.set_json_file_kv(utils.CONFIG_NAME, "project_id", uid)

# find the asset folder, project type (ios or android) and file type (vector or bitmap) 
# or select them
def resolve_project_details():
    asset_folder = utils.get_json_file_kv(utils.CONFIG_NAME, "asset_folder")
    project_type = utils.get_json_file_kv(utils.CONFIG_NAME, "project_type") 
    file_type    = utils.get_json_file_kv(utils.CONFIG_NAME, "file_type")
    last_pull_time  = utils.get_json_file_kv(utils.CONFIG_NAME, "file_type")

    if not project_type:
        project_type =  project_type_picker()

    if not asset_folder:
        if utils.PROJECT_TYPES['FLAT'] == project_type:
            asset_folder = "."

        if utils.PROJECT_TYPES['IOS'] == project_type:
            print "Searching for Xcode asset catalogues (.xcassets)"
            xcassets = [x[0] for x in os.walk(".") if x[0].endswith('.xcassets')]
            if len(xcassets) > 0:
                asset_folder = yesno_asset_folder(xcassets, utils.PROJECT_TYPES['IOS'])
            else:
                print "Couldn't autodetect any Xcode asset catalogues"

        if utils.PROJECT_TYPES['ANDROID'] == project_type:
            print "Searching for Android resource folders (*/main/res)"
            res = [x[0] for x in os.walk(".") if x[0].endswith(utils.ANDROID_RES)]
            if len(res) > 0:
                asset_folder = yesno_asset_folder(res, utils.PROJECT_TYPES['ANDROID'])
            else:
                print "Couldn't autodetect any Android resource folders"

        if not asset_folder:
            while True:
                i = raw_input("Would you like to download assets into the current folder? (y/n) ")
                if i.lower() in ('yes','y'):
                    asset_folder = "."
                    break;
                elif i.lower() in ('no','n'): sys.exit(0)

    if asset_folder:
        utils.set_json_file_kv(utils.CONFIG_NAME, "asset_folder", asset_folder)
    if project_type:
        utils.set_json_file_kv(utils.CONFIG_NAME, "project_type", project_type)
    if not file_type:
        utils.set_json_file_kv(utils.CONFIG_NAME, "file_type", "png")

    return (asset_folder, project_type, file_type)

def file_type_picker():
    print "What type of files would you like to download?"
    print "1 - bitmap (PNG) e.g. 1x,2x,mdpi,hdpi etc."
    print "2 - vector (SVG)"
    print "3 - vector (PDF)"
    number = raw_input("Please enter the option number: ")
    if number.isdigit() and int(number) <= 3 and int(number) > 0:
        if int(number) == 1:
            return "png"
        if int(number) == 2:
            return "svg"
        if int(number) == 3:
            return "pdf"
    else:
        print "Not a valid choice, try again"
        return file_type_picker()

def project_type_picker():
    print "What type of project would you like to download assets for?"
    print "1 - Current Folder / Web"
    print "2 - Android"
    print "3 - iOS"
    number = raw_input("Please enter the option number: ")
    if number.isdigit() and int(number) <= 3 and int(number) > 0:
        if int(number) == 1:
            return utils.PROJECT_TYPES['FLAT']
        if int(number) == 2:
            return utils.PROJECT_TYPES['ANDROID']
        if int(number) == 3:
            return utils.PROJECT_TYPES['IOS']
    else:
        print "Not a valid choice, try again"
        return project_type_picker()

def yesno_asset_folder(asset_folders, proj_type):
    print "Where would you like to download assets?"
    platform = "(web/platform-agnostic)"
    if proj_type == utils.PROJECT_TYPES['IOS']:
        platform = "iOS"
    elif proj_type == utils.PROJECT_TYPES['ANDROID']:
        platform = "Android"
    for i, f in enumerate(asset_folders, start=1):
        print str(i) + ". " + f + " (" + platform + ")"
    # TODO: support proper current folder download
    # current_folder_index = len(asset_folders) + 1
    # print str(current_folder_index) + ". Download assets into the current folder" + " (web/platform-agnostic)"
    number = raw_input("Please enter the option number: ")
    if number.isdigit() and int(number) <= len(asset_folders):
        return asset_folders[int(number)-1]
    if number.isdigit() and int(number) == current_folder_index:
        return "."
    else:
        print "\nCouldn't recognise that number, please try again."
        return yesno_asset_folder(asset_folders, proj_type)


