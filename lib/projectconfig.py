#!/usr/bin/env python 

import os, json
import sys, utils, urllib, apitoken as API_TOKEN

def get_project_id():
    return utils.get_json_file_kv(utils.CONFIG_NAME, "project_id")

def set_project_id(uid):
    return utils.set_json_file_kv(utils.CONFIG_NAME, "project_id", uid)

# find the asset folder, project type (ios or android) and file type (vector or bitmap) 
# or select them
def resolve_project_details():
    asset_folder = utils.get_json_file_kv(utils.CONFIG_NAME, "asset_folder")
    project_type = utils.get_json_file_kv(utils.CONFIG_NAME, "project_type") 
    file_type    = utils.get_json_file_kv(utils.CONFIG_NAME, "file_type")
    if not asset_folder or not project_type:
        res = [x[0] for x in os.walk(".") if x[0].endswith('main/res')]
        xcassets = [x[0] for x in os.walk(".") if x[0].endswith('.xcassets')]
        if len(xcassets) > 0:
            project_type = utils.PROJECT_TYPES['IOS']
            asset_folder = yesno_asset_folder(xcassets, utils.PROJECT_TYPES['IOS'])
        elif len(res) > 0:
            project_type = utils.PROJECT_TYPES['ANDROID']
            asset_folder = yesno_asset_folder(res, utils.PROJECT_TYPES['ANDROID'])
        else:
            print "Couldn't find an Xcode asset catalog (.xcassets) or Android resource folder (main/res)"
            while True:
                i = raw_input("Would you like to download assets into the current folder? (y/n) ")
                if i.lower() in ('yes','y'):
                    project_type = utils.PROJECT_TYPES['IOS']
                    asset_folder = "."
                    break;
                elif i.lower() in ('no','n'): sys.exit(0)
        if asset_folder:
            utils.set_json_file_kv(utils.CONFIG_NAME, "asset_folder", asset_folder)
        if project_type:
            utils.set_json_file_kv(utils.CONFIG_NAME, "project_type", project_type)
    if not file_type:
        file_type = file_type_picker()
        utils.set_json_file_kv(utils.CONFIG_NAME, "file_type", file_type)

    return (asset_folder, project_type, file_type)

def file_type_picker():
    print "What type of files would you like to download?"
    print "1 - bitmap (PNG) e.g. 1x,2x,mdpi,hdpi etc."
    print "2 - vector (SVG)"
    print "3 - vector (PDF)"
    number = raw_input("Please enter the option number: ")
    if number.isdigit() and int(number) <= 3 and int(number) > 0:
        if int(number) == 1:
            return "png"
        if int(number) == 2:
            return "svg"
        if int(number) == 3:
            return "pdf"
    else:
        print "Not a valid choice, try again"
        return file_type_picker()

def enter_asset_folder():
    p = raw_input("Please enter the relative path of the directory where you would like to download assets:\n")
    if not os.path.isdir(p):
        print utils.COLOR_CODES.FAIL + p + " doesn't seem to exist." + utils.COLOR_CODES.ENDC
        return enter_asset_folder()
    else:
        return p

def yesno_asset_folder(asset_folders, proj_type):
    print "Where would you like to download assets?"
    platform = "(web/platform-agnostic)"
    if proj_type == utils.PROJECT_TYPES['IOS']:
        platform = "iOS"
    elif proj_type == utils.PROJECT_TYPES['ANDROID']:
        platform = "Android"
    for i, f in enumerate(asset_folders, start=1):
        print str(i) + ". " + f + " (" + platform + ")"
    current_folder_index = len(asset_folders) + 1
    print str(current_folder_index) + ". Download assets into the current folder" + " (web/platform-agnostic)"
    number = raw_input("Please enter the option number: ")
    if number.isdigit() and int(number) <= len(asset_folders):
        return asset_folders[int(number)-1]
    if number.isdigit() and int(number) == current_folder_index:
        return "."
    else:
        print "\nCouldn't recognise that number, please try again."
        return yesno_asset_folder(asset_folders, proj_type)


