#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import urllib, urllib2, sys, json, getpass, os, cgi, traceback, \
    time, argparse, re, operator, base64, math
from urllib2 import Request, urlopen, URLError
from subprocess import call

import projectconfig, utils, apitoken as API_TOKEN, pullhandler

class FlatPullHandler(pullhandler.PullHandler):

    def __init__(self,asset_folder,file_type,proj_type):

        super(FlatPullHandler, self).__init__(asset_folder,file_type,proj_type) 

    def get_asset_name(self,tag):
        suffix = ""
        if self.config["add-id-suffix"]:
            suffix = "_"+str(tag['id'])

        return tag['name'] + suffix

    def write_asset_file(self,screen_name,asset_name,tag,response,extension,\
        useRetina4Catalogue,shared_asset):

        filename = self.get_asset_name(tag) + extension

        screen_dir = ("Shared" if shared_asset else screen_name)
        full_dir = os.path.join(self.asset_folder,screen_dir)

        fourPath = os.path.join(full_dir, filename)

        if not os.path.exists(full_dir):
            utils.mkdir_p(full_dir)

        with open(fourPath, "wb") as local_file:
            local_file.write(response.read())

        resolutions = ["1","2","3"]
        for res in resolutions:
            filename = asset_name +"@" + res + "x" + extension
            full = os.path.join(full_dir, filename)

            factor = (float(res) / 4) * 100
            call([utils.CONVERT_BIN,fourPath,
                "-alpha","on","-background","transparent",
                "-channel","RGBA"
                ,"-resize",str(factor)+"%",full])

            if self.config['optimise-assets']:
                utils.png_optimise(full)

        # clean up 4x resolution as not used in iOS (TODO: fetche 3x resolution instead for iOS?)
        os.remove(fourPath)

    def try_add_shape(self,screen,tag):
        return None  

    def add_image_layout(self,screen,tag):
        return

    def add_button_layout(self,screen,tag,bgShape=None):
        return

    def add_text_layout(self,screen,tag):
        return

    def begin_pull(self):
        return

    def add_designer_colors(self):
        return

    def finalise_pull(self):
        return        

    def finalise_screen(self,screen):
        return

    def should_fetch_resolution(self,resolution):
        return True
