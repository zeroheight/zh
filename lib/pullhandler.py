#!/usr/bin/env python 
# -*- coding: utf-8 -*-
import urllib, urllib2, sys, json, getpass, os, cgi, traceback, \
    dateutil.parser, time, argparse, re, sys, pickle, time
from urllib2 import Request, urlopen, URLError

import projectconfig, utils, apitoken as API_TOKEN
import thirdparty.webcolors as webcolors

class PullHandler(object):

    __assets_downloaded = 0
    __shapes_created = 0
    layout_details  = None
    layout_ordering = None

    def __init__(self,asset_folder,file_type):
        self.asset_folder  = asset_folder
        self.file_type     = file_type

    def pull(self, project_uid):
        if os.path.exists('.zhhistory'):
            zhhistory = open('.zhhistory', 'rb')
            previousPull = pickle.load(zhhistory)

            url = "http://" + utils.API_HOST + "/api/projects/" +\
                urllib.quote(project_uid) + "/changes"

            print "Calculating changes..."
            
            utils.api_post_request(url, previousPull, self.__changes_response, API_TOKEN.token)
        else:
            url = "http://" + utils.API_HOST + "/api/projects/" +\
                urllib.quote(project_uid) + "/tags"

            utils.api_get_request(url, self.__fresh_pull_response, API_TOKEN.token)


    def __changes_response(self,response):
        if response['changeset_id']:
            print "Found " + self.__pluralise("change",response['changes'])
            print "Review them at http://"+utils.API_HOST+"/changes/"+response['changeset_id'] + u'  <- hold ⌘ (command) and double click the URL to open it' 
            print "then run 'zh changes "+ response['changeset_id']+"' to merge the changes"
        else:
            print "Already up-to-date"        

    def __fresh_pull_response(self,response):

        response = json.loads(response.read().decode('utf-8'))

        self.begin_pull()

        print utils.COLOR_CODES.HEADER + "Project has " + self.__pluralise("screen",1) + utils.COLOR_CODES.ENDC

        for screen in response["screens"]:
            if response["screens"][screen]:
                self.process_screen(response["screens"][screen])

        self.finalise_pull()

        # write response to .zhhistory
        with open(".zhhistory", 'w') as zhhistory:
            pickle.dump(response,zhhistory)

    def process_screen(self,screen):
        print utils.COLOR_CODES.UNDERLINE +"Screen '" + screen['name'] + "':"+ utils.COLOR_CODES.ENDC

        self.__fetch_screen_preview(screen)

        self.layout_details = {}
        self.layout_ordering = {}
        self.__assets_downloaded = 0
        self.__shapes_created = 0

        for tag in screen['tags']:
            self.__process_tag(screen,screen['tags'][tag])

        if self.__assets_downloaded > 0:
            print utils.COLOR_CODES.OKGREEN + "    Downloaded " + self.__pluralise("asset",self.__assets_downloaded)+ utils.COLOR_CODES.ENDC
        if self.__shapes_created > 0:
            print utils.COLOR_CODES.OKGREEN + "    Created " + self.__pluralise("shape",self.__shapes_created)+ utils.COLOR_CODES.ENDC

        self.finalise_screen(screen)

    def __process_tag(self,screen,tag):
        if tag['tag_type'] == "image":
            self.__process_image_tag(screen,tag)
        if tag['tag_type'] == 'button':
            self.__process_button_tag(screen,tag)
        if tag['tag_type'] == "shape":
            self.__process_shape_tag(screen,tag)
        if tag['tag_type'] == "text":
            self.__process_text_tag(screen,tag)

    def __process_shape_tag(self,screen,tag):
        result = self.try_add_shape(screen,tag)

        if result != None:
            self.__shapes_created += 1
            self.add_image_layout(screen,tag,result)
        else:
            # fallback to downloading image
            self.__process_image_tag(screen,tag)

    def __process_text_tag(self,screen,tag):
        self.add_text_layout(screen,tag)

    def __process_button_tag(self,screen,tag):
        bgShape = None
        for component_role in tag['components']:
            component = tag['components'][component_role]

            if component_role == "background":
                component['name'] = tag['name']+"_background"

                if component['layer_type'] == "shape":
                    bgShape = self.try_add_shape(screen,component)

                if bgShape is not None:
                    self.__shapes_created += 1
                else:
                    self.download_item_images(screen,component)
                    self.__assets_downloaded += 1                

            if component_role == "subimage":
                component['name'] = tag['name']+"_subimage"
                self.download_item_images(screen,component)
                self.__assets_downloaded += 1

            if component_role == "label":
                component['name'] = tag['name']+"_label"

        self.add_button_layout(screen,tag,bgShape)

    def __process_image_tag(self,screen,tag):
           
        self.download_item_images(screen,tag)
        self.__assets_downloaded += 1

        self.add_image_layout(screen,tag)

    def __handle_item_image(self,response,screen_name,item_name,item):
        _, params = cgi.parse_header(response.headers.get('Content-Disposition', ''))
        extension = os.path.splitext(str(params["filename"]))[-1]

        self.write_asset_file(screen_name,item_name,item,response,extension,False)


    def __format_bytes_to_readable(self,num, suffix='B'):
        for unit in ['','K','M','G']:
            if abs(num) < 1024.0:
                return "%3.1f %s%s" % (num, unit, suffix)
            num /= 1024.0
        return "%.1f %s%s" % (num, 'Yi', suffix)

    def download_item_images(self,screen,item):
        print "    Downloading image for " + item["name"] + " (" + \
            self.__format_bytes_to_readable(item["image_size"]) + ")"

        item_name = item['name']+"_"+str(item['id'])

        url = "http://" + utils.API_HOST + "/api/tags/" +\
                str(item['layer_id']) + "/image"

        utils.api_get_request(url, lambda response: \
            self.__handle_item_image(response,screen['name'],item_name,item),\
            API_TOKEN.token)

    def __pluralise(self,word,count):
        return (str(count) + " " + word + ("s" if (count != 1) else ""))        

    def __get_url_resolution(self,resolution):
        if resolution == "0.75":
            return "0%2E75"
        if resolution == "1.5":
            return "1%2E5"
        return resolution

    def __fetch_screen_preview(self,screen):
        url = "http://" + utils.API_HOST + "/api/screens/" + str(screen["id"]) + "/preview"

        def callback(response):
            # overridden by architecture specific derivative classes 
            self.write_screen_preview(screen,response)

        utils.api_get_request(url, callback, API_TOKEN.token)

    # these methods are overridden by architecture-specific derivative classes

    def add_button_layout(self,screen,tag):
        raise NotImplementedError

    def add_text_layout(self,screen,tag):
        raise NotImplementedError

    def add_image_layout(self,screen,tag):
        raise NotImplementedError

    def write_screen_preview(self,screen_name,response,asset_folder):
        raise NotImplementedError

    def should_fetch_resolution(self):
        raise NotImplementedError

    def write_asset_file(self,screen_name,asset_name,\
        resolution,response,extension,useRetina4Catalogue,asset_folder):
        raise NotImplementedError

    def begin_pull(self,asset_folder):
        raise NotImplementedError

    def finalise_pull(self,asset_folder):
        raise NotImplementedError

    def finalise_screen(self,screen,asset_folder):
        raise NotImplementedError
