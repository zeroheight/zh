#!/usr/bin/env python 
# -*- coding: utf-8 -*-
import urllib, urllib2, sys, json, getpass, os, cgi, traceback, \
    time, argparse, re, sys, pickle, time, shutil
from urllib2 import Request, urlopen, URLError

import projectconfig, utils, apitoken as API_TOKEN
import thirdparty.webcolors as webcolors

class PullHandler(object):

    __assets_downloaded = 0
    __shapes_created = 0
    layout_details  = {}
    layout_ordering = {}
    shared_hashes = {}
    asset_hashes = {}
    designer_colors = {}
    config = {}

    def __init__(self,asset_folder,file_type,proj_type):
        self.asset_folder  = asset_folder
        self.file_type     = file_type
        self.proj_type     = proj_type

    def pull(self, project_uid, args, last_pull_time):
        self.icon_folder     = args.icon_folder

        url = "https://" + utils.API_HOST + "/api/projects/" +\
            urllib.quote(project_uid) + "/pull"

        if last_pull_time and not args.force:
            print "Calculating changes..."
            url += "?last_pull_time=" + str(last_pull_time)
            utils.api_get_request(url, self.__changes_response, API_TOKEN.token)
        else:
            utils.api_get_request(url, self.__fresh_pull_response, API_TOKEN.token)

    def __changes_response(self,response):
        response = json.loads(response.read().decode('utf-8'))
        
        self.parse_config(response,False)

        self.begin_pull()

        if "colors" in response:
            self.designer_colors = response["colors"]
            if self.proj_type == utils.PROJECT_TYPES['ANDROID']:
                self.add_designer_colors()

        tagCount = 0

        for screen_id in response["screens"]:
            if response["screens"][screen_id]:
                screen = response["screens"][screen_id]
                for tag_name in screen['tags']:
                    tagCount += 1

        if tagCount > 0:
            if (
            (self.proj_type == utils.PROJECT_TYPES['ANDROID'] and self.config['android-drawables']) or
            (self.proj_type == utils.PROJECT_TYPES['IOS'] and self.config['ios-assets']) or
            self.proj_type == utils.PROJECT_TYPES['FLAT']
            ):
                self.process_assets(response["screens"])

        changeCount = self.__assets_downloaded + self.__shapes_created

        if self.proj_type == utils.PROJECT_TYPES["ANDROID"]:
            changeCount += self.color_manager.added

        self.finalise_pull()

        if changeCount == 0:
            print utils.COLOR_CODES.OKGREEN + "Looks like you're already up-to-date" + utils.COLOR_CODES.ENDC
            print "If you want to force re-download, use '"+utils.COLOR_CODES.WARNING+"zh pull -f"+utils.COLOR_CODES.ENDC+"'"

        projectconfig.set_last_pull_time(response["pull_time"])

    def __fresh_pull_response(self,response):

        response = json.loads(response.read().decode('utf-8'))

        print utils.COLOR_CODES.HEADER + utils.COLOR_CODES.UNDERLINE + "Project '" + response["project_name"] + "' : " +\
            self.__pluralise("screen",len(response["screens"])) + utils.COLOR_CODES.ENDC

        self.parse_config(response)

        self.begin_pull()

        if "shared_hashes" in response:
            self.shared_hashes = response["shared_hashes"]

        # tentatively add all the designer colors, they'll only get written
        # to file if colors are enabled
        if "colors" in response:
            self.designer_colors = response["colors"]
            if self.proj_type == utils.PROJECT_TYPES['ANDROID']:
                self.add_designer_colors()

        # tentatively add the project strings, they'll only get written 
        # to file if strings are enabled
        if (self.proj_type == utils.PROJECT_TYPES['ANDROID'] and self.config['android-strings']):
            for screen in response["screens"]:
                if response["screens"][screen]:
                    for tag_id in response["screens"][screen]['tags']:
                        tag = response["screens"][screen]['tags'][tag_id]
                        if tag['tag_type'] == "text":
                            s = tag['styles']
                            styles = json.loads(s.replace("=>",":"))
                            styles = styles['sections'][0] if ("sections" in styles) else styles
                            self.string_manager.find_or_create_id(\
                                styles['text'].replace('\n', '\\n'))
                        elif tag['tag_type'] == "button":
                            if "label" in tag["components"]:
                                s = tag["components"]['label']['styles']
                                styles = json.loads(s.replace("=>",":"))
                                styles = styles['sections'][0] if ("sections" in styles) else styles
                                self.string_manager.find_or_create_id(\
                                    styles['text'].replace('\n', '\\n'))

        # download assets if enabled
        if (
            (self.proj_type == utils.PROJECT_TYPES['ANDROID'] and self.config['android-drawables']) or
            (self.proj_type == utils.PROJECT_TYPES['IOS'] and self.config['ios-assets']) or
            self.proj_type == utils.PROJECT_TYPES['FLAT']
            ):
            self.process_assets(response["screens"])

        # write layouts if they're enabled
        if self.proj_type == utils.PROJECT_TYPES['ANDROID'] and self.config['android-layouts']:
            print utils.COLOR_CODES.UNDERLINE + "Writing Layouts" + utils.COLOR_CODES.ENDC

            print utils.COLOR_CODES.WARNING + "    NOTE : layouts "+\
            "are very experimental.\n           Take a "+\
            "look and give us feedback!\n           You can turn them off from the project web page." \
            + utils.COLOR_CODES.ENDC

            for screen in response["screens"]:
                if response["screens"][screen]:
                    self.process_screen_layout(response["screens"][screen])

            print utils.COLOR_CODES.OKGREEN + "    Processed "+ \
                self.__pluralise("screen",len(response["screens"]))+ utils.COLOR_CODES.ENDC

        if len(response["screens"]) > 0:
            self.finalise_pull()

        if os.path.isfile('.zhhistory'):
            os.remove(".zhhistory")

        if os.path.isdir('.zhthumbnails'):
            shutil.rmtree(".zhthumbnails")

        if os.path.isdir('zeroheight_screen_previews'):
            shutil.rmtree("zeroheight_screen_previews")

        projectconfig.set_last_pull_time(response["pull_time"])


    def parse_config(self,response,verbose=True):
        got_config = ("config" in response)
        settings = []
        if self.proj_type == utils.PROJECT_TYPES['ANDROID']:
            settings = ["android-drawables","android-layouts","android-colors","android-strings"]
        if self.proj_type == utils.PROJECT_TYPES['IOS']:
            settings = ["ios-assets"]
        
        settings.extend(["optimise-assets","use-category-folders","add-id-suffix"])

        if verbose:
            print utils.COLOR_CODES.UNDERLINE + "Settings" + utils.COLOR_CODES.ENDC
            print "You can change these from the 'Developer Resources' tab on the Project page"

        for setting in settings:
            self.config[setting] = True

            if (("config" in response) and (setting in response['config'])):
                self.config[setting] = True if (response['config'][setting] == "1") else False
            if verbose and setting != "use-category-folders" and setting != "add-id-suffix":
                print "    " + self.__display_setting(setting) + " : " + ("ON" if self.config[setting] else "OFF")

    def __display_setting(self,setting):
        if setting == "optimise-assets":
            return "Compress PNGs".ljust(17)

        return setting.ljust(17)

    def process_assets(self,screens):

        print utils.COLOR_CODES.UNDERLINE + "Downloading Assets" + utils.COLOR_CODES.ENDC

        for screen_id in screens:
            if screens[screen_id]:
                screen = screens[screen_id]

                print utils.COLOR_CODES.HEADER + "    Processing screen '" + screen['name'] + "'" +\
                    utils.COLOR_CODES.ENDC

                for tag_name in screen['tags']:
                    tag = screen['tags'][tag_name]
                    self.process_tag_assets(tag,screen)
        
        if self.__assets_downloaded > 0:
            print utils.COLOR_CODES.OKGREEN + "    Downloaded " + \
                self.__pluralise("asset",self.__assets_downloaded)+ utils.COLOR_CODES.ENDC
        if self.__shapes_created > 0:
            print utils.COLOR_CODES.OKGREEN + "    Created " + \
                self.__pluralise("shape",self.__shapes_created)+ utils.COLOR_CODES.ENDC

    def process_tag_assets(self,tag,screen):
        asset_hash = tag['image_hash'] if tag['image_hash'] else self.get_asset_name(tag)
        if tag['tag_type'] == "shape":
            if asset_hash not in self.asset_hashes:

                shared_asset = False
                if asset_hash in self.shared_hashes:
                    shared_asset = True

                result = self.try_add_shape(screen,tag)
                if result != None:
                    self.__shapes_created += 1
                    print "    Created shape " + result
                    self.asset_hashes[asset_hash] = result
                else:
                    self.download_item_image(screen,tag,tag['category'],shared_asset)
                    self.__assets_downloaded += 1
                    self.asset_hashes[asset_hash] = self.get_asset_name(tag)
        elif tag['tag_type'] == "image":
            if asset_hash not in self.asset_hashes:

                shared_asset = False
                if asset_hash in self.shared_hashes:
                    shared_asset = True

                self.download_item_image(screen,tag,tag['category'],shared_asset)
                self.__assets_downloaded += 1
                self.asset_hashes[asset_hash] = self.get_asset_name(tag)
        elif tag['tag_type'] == "button":
            self.process_button_assets(tag,screen)

    def process_button_assets(self,tag,screen):
        bgShape = None
        for component_role in tag['components']:
            component = tag['components'][component_role]

            if component_role == "background":
                component['name'] = tag['name']
                component['category'] = "button"

                asset_hash = component['image_hash'] if \
                    component['image_hash'] else \
                    component['name']+str(component['id'])

                if asset_hash not in self.asset_hashes:

                    shared_asset = False
                    if asset_hash in self.shared_hashes:
                        shared_asset = True

                    if component['layer_type'] == "shape":
                        bgShape = self.try_add_shape(screen,component)

                    if bgShape is not None:
                        print "    Created shape " + bgShape
                        self.asset_hashes[asset_hash] = bgShape
                        self.__shapes_created += 1
                    else:
                        self.download_item_image(screen,component,"button",shared_asset)
                        self.asset_hashes[asset_hash] = self.get_asset_name(component)
                        self.__assets_downloaded += 1

            if component_role == "label":
                component['name'] = tag['name']+"_label"

    def process_screen_layout(self,screen):
        self.layout_details = {}
        self.layout_ordering = {}

        for tag in screen['tags']:
            self.process_tag(screen,screen['tags'][tag])

        self.finalise_screen(screen)

    def process_tag(self,screen,tag):
        if tag['category'] == "button":
            self.__process_button_tag(screen,tag)
        elif tag['tag_type'] == "image":
            self.__process_image_tag(screen,tag)
        elif tag['tag_type'] == 'button':
            self.__process_button_tag(screen,tag)
        elif tag['tag_type'] == "shape":
            self.__process_shape_tag(screen,tag)
        elif tag['tag_type'] == "text":
            self.__process_text_tag(screen,tag)

    def __process_shape_tag(self,screen,tag):
        self.add_image_layout(screen,tag)

    def __process_text_tag(self,screen,tag):
        self.add_text_layout(screen,tag)

    def __process_button_tag(self,screen,tag):
        self.add_button_layout(screen,tag)

    def __process_image_tag(self,screen,tag):
        self.add_image_layout(screen,tag)

    def __handle_item_image(self,response,screen_name,item_name,item,shared_asset):
        _, params = cgi.parse_header(response.headers.get('Content-Disposition', ''))
        extension = os.path.splitext(str(params["filename"]))[-1]

        self.write_asset_file(screen_name,item_name,item,response,extension,False,shared_asset)

    def download_item_image(self,screen,item,image_type,shared_asset):
        thing = image_type if image_type else "image"
        thing = ("shared " + thing) if shared_asset else thing

        print "    Downloading " + thing + " "+ self.get_asset_name(item) + " (" + \
            utils.format_bytes_to_readable(item["image_size"]) + ")"

        item_name = self.get_asset_name(item)    

        url = "https://" + utils.API_HOST + "/api/tags/" +\
                str(item['layer_id']) + "/image"

        utils.api_get_request(url, lambda response: \
            self.__handle_item_image(response,screen['name'],item_name,item,shared_asset),\
            API_TOKEN.token)

    def __pluralise(self,word,count):
        return (str(count) + " " + word + ("s" if (count != 1) else ""))        

    def __get_url_resolution(self,resolution):
        if resolution == "0.75":
            return "0%2E75"
        if resolution == "1.5":
            return "1%2E5"
        return resolution

    # these methods are overridden by architecture-specific derivative classes

    def add_button_layout(self,screen,tag):
        raise NotImplementedError

    def add_text_layout(self,screen,tag):
        raise NotImplementedError

    def add_image_layout(self,screen,tag):
        raise NotImplementedError

    def should_fetch_resolution(self):
        raise NotImplementedError

    def write_asset_file(self,screen_name,asset_name,\
        tag,response,extension,useRetina4Catalogue):
        raise NotImplementedError

    def begin_pull(self):
        raise NotImplementedError

    def finalise_pull(self):
        raise NotImplementedError

    def finalise_screen(self,screen):
        raise NotImplementedError
