#!/usr/bin/env python 
import argparse, sys
from argparse import RawTextHelpFormatter

import commandshandler

class ArgumentHandler:

    def __init__(self):
        self.__commandshandler = commandshandler.CommandsHandler()

        self.desc = """==========================================================================
 zh is a utility that enables you to fetch designs uploaded to zeroheight
=========================================================================="""
        self.parser = argparse.ArgumentParser(formatter_class=RawTextHelpFormatter)
        subparsers = self.parser.add_subparsers()

        pull_help = """<<project ID>>   - pull the latest design information uploaded by the designers
-f/--force  - re-download everything rather than only what has changed since the last pull

    e.g. zh pull kngz5oyte9d17fod3lav
         zh pull kngz5oyte9d17fod3lav -f
        """

        pull_parser = subparsers.add_parser('pull',help=pull_help,formatter_class=RawTextHelpFormatter)
        pull_parser.add_argument("pull_id", nargs="?")
        pull_parser.add_argument('-f', '--force', action='store_true')
        pull_parser.add_argument('-if', '--icon-folder')
        pull_parser.set_defaults(func=self.__commandshandler.pull)

        clean_help = """removes all zeroheight resources, ready for a fresh pull"""

        clean_parser = subparsers.add_parser('clean',help=clean_help,formatter_class=RawTextHelpFormatter)
        clean_parser.set_defaults(func=self.__commandshandler.clean)
        clean_parser.add_argument('-f', '--force', action='store_true')


    def handle_args(self):
        if len(sys.argv) < 2:
            print self.desc
            self.parser.print_help()
            sys.exit(1)

        args = self.parser.parse_args()
        args.func(args)
