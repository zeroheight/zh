#!/usr/bin/env python 
import argparse, sys
from argparse import RawTextHelpFormatter

import commandshandler

class ArgumentHandler:

    def __init__(self):
        self.__commandshandler = commandshandler.CommandsHandler()

        self.desc = """========================================================================
    zh is a utility that enables you to fetch designs uploaded to zeroheight
========================================================================"""
        self.parser = argparse.ArgumentParser(formatter_class=RawTextHelpFormatter)
        subparsers = self.parser.add_subparsers()

        pull_help = """<<project ID>> - pull the latest design information uploaded by the designers

    e.g. zh pull kngz5oyte9d17fod3lav
        """

        pull_parser = subparsers.add_parser('pull',help=pull_help,formatter_class=RawTextHelpFormatter)
        pull_parser.add_argument("pull_id", nargs="?")
        pull_parser.set_defaults(func=self.__commandshandler.pull)


        changes_help = """<<changeset ID>> - update the project with the changes from the chosen changeset

    e.g. zh changes fcf23e34558af990306d4a1b605e5f9c
        """

        pull_parser = subparsers.add_parser('changes',help=changes_help,formatter_class=RawTextHelpFormatter)
        pull_parser.add_argument("changeset_id")
        pull_parser.set_defaults(func=self.__commandshandler.changes)


    def handle_args(self):
        if len(sys.argv) < 2:
            print self.desc
            self.parser.print_help()
            sys.exit(1)

        args = self.parser.parse_args()
        args.func(args)
