#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import os
import xml.etree.ElementTree as ET

import utils

import thirdparty.webcolors as webcolors
import thirdparty.xmlpp as xmlpp
from StringIO import StringIO
import re

class PIParser(ET.XMLTreeBuilder):

   def __init__(self):
       ET.XMLTreeBuilder.__init__(self)
       # assumes ElementTree 1.2.X
       self._parser.CommentHandler = self.handle_comment
       self._parser.ProcessingInstructionHandler = self.handle_pi
       self._target.start("document", {})

   def close(self):
       self._target.end("document")
       return ET.XMLTreeBuilder.close(self)

   def handle_comment(self, data):
       self._target.start(ET.Comment, {})
       self._target.data(data)
       self._target.end(ET.Comment)

   def handle_pi(self, target, data):
       self._target.start(ET.PI, {})
       self._target.data(target + " " + data)
       self._target.end(ET.PI)

def special_parse(source):
    return ET.parse(source, PIParser())

class AndroidXMLs():
    def __init__(self):
        self.names = set([])
        self.value_to_name = {}
        self.elements = []
        self.added = 0

    def process_value(self,value):
        return value

    def get_id(self,value):
        return value

    def read_xml(self,asset_folder):
        full_dir = os.path.join(asset_folder, "values")
        path = os.path.join(full_dir, self.xml_path)
        if os.path.isfile(path):
            try:
                tree = special_parse(path)
                self.resources = tree.getroot()[0]

                for element in self.resources:
                    name   = element.get("name")
                    value  = self.process_value(element.text)
                    if name is not None:
                        self.names.add(name)
                        self.value_to_name[value] = name
            except:
                self.resources = ET.Element("resources")
        else:
            self.resources = ET.Element("resources")


    def count_elements_with_prefix(self,prefix):
        count = 0 
        for element in self.resources:
            name = element.get("name")
            if name and name.startswith(prefix):
                count += 1
        return count

    def remove_elements_with_prefix(self,prefix):
        deletes = []
        for element in self.resources:
            name = element.get("name")
            if name and name.startswith(prefix):
                deletes.append(element)
        for delete in deletes:
            self.resources.remove(delete)

    def find_or_create_or_hardcode(self,value,presets={}):
        orig = value.upper()
        value = self.process_value(value)

        if value in self.value_to_name:
            return "@" + self.element_tag + "/" + self.value_to_name[value]
        else:
            if orig in presets:
                return "@" + self.element_tag + "/" + self.find_or_create_id(value,presets)
            else:
                return value

    def find_or_create_id(self,value,presets={}):
        orig = value.upper()
        value = self.process_value(value)

        if value in self.value_to_name:
            return self.value_to_name[value]
        else:
            chosen_name = first_name = self.get_id(value)
            if orig in presets:
                chosen_name = first_name =  "zh_"+utils.androidify_name(presets[orig])
            suffix = 1
            while chosen_name in self.names:
                chosen_name = first_name + str(suffix)
                suffix = suffix + 1

            self.names.add(chosen_name)
            self.value_to_name[value] = chosen_name

            c = ET.SubElement(self.resources, self.element_tag)
            c.set("name",chosen_name)
            c.text = value
            self.added += 1

            return chosen_name

    def write_xml(self,asset_folder):
        full_dir = os.path.join(asset_folder, "values")

        if not os.path.exists(full_dir):
            utils.mkdir_p(full_dir)

        path = os.path.join(full_dir, self.xml_path)

        print utils.COLOR_CODES.OKGREEN +"    Updating " + path + utils.COLOR_CODES.ENDC

        with open(path, 'w') as f:
            f.write ('<?xml version="1.0" encoding="utf-8"?>\n')
            output = StringIO()
            xmlpp.pprint(ET.tostring(self.resources), output=output, indent=4, width=160)
            lUglyXml = output.getvalue()
            text_re = re.compile('>\n\s+([^<>\s].*?)\n\s+</', re.DOTALL)    
            lPrettyXml = text_re.sub('>\g<1></', lUglyXml)
            f.write(lPrettyXml)

class AndroidColors(AndroidXMLs):
    def __init__(self):
        AndroidXMLs.__init__(self)
        self.xml_path = "colors.xml"
        self.element_tag = "color"
        self.hex_reg = re.compile('^#[0-9a-f]+$')

    def process_value(self,value):
        value = value.strip().lower().encode('ascii', 'ignore')
        result = self.hex_reg.match(value)
        if result is None:
            return "#000000"
        else:
            return value

    def get_id(self,value):
        return "zh_"+webcolors.hex_to_closest_name(value)

class AndroidStrings(AndroidXMLs):
    def __init__(self):
        AndroidXMLs.__init__(self)
        self.xml_path = "strings.xml"
        self.element_tag = "string"

    def process_value(self,value):
        return value.strip()

    def get_id(self,value):
        return "zh_"+utils.androidify_name(value)
