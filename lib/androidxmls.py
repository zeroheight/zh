#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import os
import xml.etree.ElementTree as ET

import utils

import thirdparty.webcolors as webcolors
import thirdparty.xmlpp as xmlpp

class AndroidXMLs():
    def __init__(self):
        self.names = set([])
        self.value_to_name = {}
        self.elements = []

    def process_value(self,value):
        return value

    def get_id(self,value):
        return value

    def read_xml(self,asset_folder):
        full_dir = os.path.join(asset_folder, "values")
        path = os.path.join(full_dir, self.xml_path)
        if os.path.isfile(path):
            try:
                tree = ET.parse(path)
                resources = tree.getroot()

                for element in resources:
                    name   = element.get("name")
                    value  = self.process_value(element.text)
                    self.names.add(name)
                    self.value_to_name[value] = name
                    self.elements.append({'name':name,'value':value})
            except:
                pass

    def find_or_create_id(self,value):
        value = self.process_value(value)

        if value in self.value_to_name:
            return self.value_to_name[value]
        else:
            chosen_name = first_name = self.get_id(value)
            suffix = 1
            while chosen_name in self.names:
                chosen_name = first_name + str(suffix)
                suffix = suffix + 1

            self.names.add(chosen_name)
            self.value_to_name[value] = chosen_name
            self.elements.append({'name':chosen_name,'value':value})
            return chosen_name

    def write_xml(self,asset_folder):

        full_dir = os.path.join(asset_folder, "values")

        if not os.path.exists(full_dir):
            utils.mkdir_p(full_dir)

        path = os.path.join(full_dir, self.xml_path)

        print utils.COLOR_CODES.OKGREEN +"Updating " + path + utils.COLOR_CODES.ENDC

        if len(self.elements) > 0:
            resources = ET.Element("resources")
            for element in self.elements:
                c = ET.SubElement(resources, self.element_tag)
                c.set("name",element['name'])
                c.text = element['value']

            with open(path, 'w') as f:
                f.write ('<?xml version="1.0" encoding="utf-8"?>\n')
                xmlpp.pprint(ET.tostring(resources),output=f)

class AndroidColors(AndroidXMLs):
    def __init__(self):
        AndroidXMLs.__init__(self)
        self.xml_path = "colors.xml"
        self.element_tag = "color"

    def process_value(self,value):
        return value.strip().lower().encode('ascii', 'ignore')

    def get_id(self,value):
        return webcolors.hex_to_closest_name(value)

class AndroidStrings(AndroidXMLs):
    def __init__(self):
        AndroidXMLs.__init__(self)
        self.xml_path = "strings.xml"
        self.element_tag = "string"

    def process_value(self,value):
        return value.strip()

    def get_id(self,value):
        return utils.androidify_name(value)
