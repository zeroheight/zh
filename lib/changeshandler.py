#!/usr/bin/env python 
# system
import urllib,json,os,pickle,sys

# local
import utils, apitoken as API_TOKEN

class ChangesHandler:

    def __init__(self,pullhandler):
        self.__pullhandler = pullhandler

    def handle_changeset(self,id):
        url = "https://" + utils.API_HOST + "/api/changesets/" +urllib.quote(id)
        utils.api_get_request(url, self.__handle_changeset_response, API_TOKEN.token)

    def __handle_changeset_response(self,response):
        response = json.loads(response.read().decode('utf-8'))
        newPull = response["tags"]
        changes = response["changes"]

        previousPull = None
        if os.path.exists('.zhhistory'):
            zhhistory = open('.zhhistory', 'rb')
            previousPull = pickle.load(zhhistory)
        else:
            "no .zhhistory file, please run zh pull first"
            sys.exit(0)

        self.__pullhandler.begin_pull()

        try:
            print utils.COLOR_CODES.UNDERLINE + "Downloading Changed Assets" + utils.COLOR_CODES.ENDC
            for screen in changes:
                self.__process_asset_changes(screen,changes[screen],newPull,previousPull)

            print utils.COLOR_CODES.UNDERLINE + "Merging Screen Changes" + utils.COLOR_CODES.ENDC
            for screen in changes:
                self.__process_screen_changes(screen,changes[screen],newPull,previousPull)
        except:
            print "Something went wrong. Perhaps the changeset is invalid"
            raise

        self.__pullhandler.finalise_pull()

        with open(".zhhistory", 'wb') as zhhistory:
            pickle.dump(newPull,zhhistory)


    def __process_asset_changes(self,screen_id,screen,newPull,previousPull):

        newScreen = newPull["screens"][screen_id]
        newTags = newScreen["tags"]

        # download any image changes
        for tag_id in screen["images"]:
            newTag = newTags[str(tag_id)]
            self.__pullhandler.process_tag_assets(newTag,newScreen)

        # download any background changes
        for tag_id in screen["component_backgrounds"]:
            newTag = newTags[str(tag_id)]
            component = newTag['components']["background"]
            component['name'] = newTag['name']+"_background"
            self.__pullhandler.process_tag_assets(newTag,newScreen)

        # tag has been added
        for tag_id in screen["added_tags"]:
            newTag = newTags[str(tag_id)]
            self.__pullhandler.process_tag_assets(newTag,newScreen)


    def __process_screen_changes(self,screen_id,screen,newPull,previousPull):

        if "screen_added" in screen:
            newScreen = newPull["screens"][screen_id]
            self.__pullhandler.process_screen(newScreen)

        elif "screen_deleted" in screen:
            oldScreen = previousPull["screens"][screen_id]

        else:
            newScreen = newPull["screens"][screen_id]
            print "    Merging changes for screen '" + \
                newScreen["name"] + "'"

            newTags = newScreen["tags"]

            for tag_id in screen["added_tags"]:
                newTag = newTags[str(tag_id)]
                print "        New "+newTag["tag_type"] + " element '" + newTag["name"] + "' has been added."

            # tag has been removed
            for tag_id in screen["deleted_tags"]:
                try:                    
                    oldScreen = previousPull["screens"][screen_id]
                    oldTag = oldScreen["tags"][str(tag_id)]
                    print "    Element '" + oldTag["name"] + "' has been removed"
                except:
                    pass                

            # tag has been changed
            for change_info in screen["changed_tags"]:
                print "    " + change_info["message"] + ". Please carry out changes manually"
