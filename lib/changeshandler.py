#!/usr/bin/env python 
# system
import urllib,json,os,pickle,sys

# local
import utils, apitoken as API_TOKEN

class ChangesHandler:

    def __init__(self,pullhandler):
        self.__pullhandler = pullhandler

    def handle_changeset(self,id):
        url = "http://" + utils.API_HOST + "/api/changesets/" +urllib.quote(id)
        utils.api_get_request(url, self.__handle_changeset_response, API_TOKEN.token)

    def __handle_changeset_response(self,response):
        response = json.loads(response.read().decode('utf-8'))
        newPull = response["tags"]
        changes = response["changes"]

        previousPull = None
        if os.path.exists('.zhhistory'):
            zhhistory = open('.zhhistory', 'rb')
            previousPull = pickle.load(zhhistory)
        else:
            "no .zhhistory file, please run a fresh zh pull"
            sys.exit(0)

        try:
            for screen in changes:
                self.__process_screen_changes(screen,changes[screen],newPull,previousPull)
        except:
            print "Something went wrong. Perhaps the changeset is invalid"
            raise

        with open(".zhhistory", 'w') as zhhistory:
            pickle.dump(newPull,zhhistory)

    def __process_screen_changes(self,screen_id,screen,newPull,previousPull):

        if "screen_added" in screen:
            newScreen = newPull["screens"][screen_id]
            print "New screen '" + newScreen["name"] + "' added"
            self.__pullhandler.process_screen(newScreen)

        elif "screen_deleted" in screen:
            oldScreen = previousPull["screens"][screen_id]
            print "Screen '" + oldScreen["name"] + "' deleted"
            print "    Not deleting any resources/templates"

        else:
            newScreen = newPull["screens"][screen_id]
            print "Merging changes for screen '" + newScreen["name"] + "'"

            newTags = newScreen["tags"]

            # download any image changes
            for tag_id in screen["images"]:
                newTag = newTags[str(tag_id)]
                print "    Downloading image changes for '" + newTag["name"] + "'"
                self.__pullhandler.download_item_images(newScreen,newTag)

            # download any background changes
            for tag_id in screen["component_backgrounds"]:
                newTag = newTags[str(tag_id)]
                component = newTag['components']["background"]
                component['name'] = newTag['name']+"_background"
                print "    Downloading image changes for '" + component["name"] + "'"
                self.__pullhandler.download_item_images(newScreen,component)

            # download any subimage changes
            for tag_id in screen["component_subimages"]:
                newTag = newTags[str(tag_id)]
                component = newTag['components']["subimage"]
                component['name'] = newTag['name']+"_subimage"
                print "    Downloading image changes for '" + component["name"] + "'"
                self.__pullhandler.download_item_images(newScreen,component)

            # tag has been removed
            for tag_id in screen["deleted_tags"]:
                oldScreen = previousPull["screens"][screen_id]
                oldTag = oldScreen["tags"][str(tag_id)]
                print "    Element '" + newTag["name"] + "' has been removed"

            # tag has been added
            for tag_id in screen["added_tags"]:
                newTag = newTags[str(tag_id)]
                print "    Element '" + newTag["name"] + "' has been added"

            # tag has been changed
            for change_info in screen["changed_tags"]:
                print "    " + change_info["message"] + ". Please carry out changes manually"


