#!/usr/bin/env python 

import os, json, sys
import utils

GLOBAL_CONFIG_NAME = ".zhconfig_global"
ERROR_TOKEN = "kH1AxT3NJz7Cxc"

# default value is Generic account
token = ERROR_TOKEN

def resolve(args_token):
    global token
    if args_token:
        token = args_token
    else:
        token = get_token_from_global_config()
        if not token:
            print utils.COLOR_CODES.FAIL + "No valid token found." + utils.COLOR_CODES.ENDC + "\nEither authenticate with zh auth \"token\" or use zh pull \"unique_project_id\""
            sys.exit(1)

def save(save_token):
    global_cfg_path = os.path.join(os.path.expanduser('~'), GLOBAL_CONFIG_NAME)
    utils.set_json_file_kv(global_cfg_path, 'api_token', save_token)

def get_token_from_global_config():
    global_cfg_path = os.path.join(os.path.expanduser('~'), ".zhconfig_global")
    if (os.path.isfile(global_cfg_path)):
        global_cfg_file_json = json.loads(open(global_cfg_path, "r").read())
        if 'api_token' in global_cfg_file_json:
            return global_cfg_file_json['api_token']
    return None