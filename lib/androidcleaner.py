import androidxmls, os, re, utils, shutil

class AndroidCleaner(object):
    def __init__(self,asset_folder):
        self.__asset_folder = asset_folder

        self.__color_manager  = androidxmls.AndroidColors()
        self.__string_manager = androidxmls.AndroidStrings()

    def clean(self,force):

        print utils.COLOR_CODES.UNDERLINE + "Searching Strings and Colors" + utils.COLOR_CODES.ENDC

        self.__color_manager.read_xml(self.__asset_folder)
        self.__string_manager.read_xml(self.__asset_folder)

        colors = self.__color_manager.count_elements_with_prefix("zh_")
        strings = self.__string_manager.count_elements_with_prefix("zh_")

        print "   Found " + str(colors) + " colors with 'zh_' prefix"
        print "   Found " + str(strings) + " strings with 'zh_' prefix"

        print utils.COLOR_CODES.UNDERLINE + "Searching Drawables" + utils.COLOR_CODES.ENDC

        drawables = []

        basic_regex = "(ic|btn|img)_.*_\d+\.png"
        pattern = re.compile('^'+basic_regex+'$')

        full_dir = os.path.join(self.__asset_folder,"drawable-hdpi")
        try:
            for filestr in os.listdir(full_dir):
                result = pattern.match(filestr)
                if result is not None:
                    drawables.append(filestr)
        except:
            pass

        full_dir = os.path.join(self.__asset_folder,"mipmap-xxxhdpi")
        try:
            for filestr in os.listdir(full_dir):
                result = pattern.match(filestr)
                if result is not None:
                    drawables.append(filestr)
        except:
            pass

        print "   Found " + str(len(drawables)) + " drawables matching " + basic_regex

        print utils.COLOR_CODES.UNDERLINE + "Searching Layouts" + utils.COLOR_CODES.ENDC

        layouts = []

        full_dir = os.path.join(self.__asset_folder,"layout")
        try:
            for filestr in os.listdir(full_dir):
                if filestr.startswith("zeroheight_"):
                    layouts.append(filestr)
            print "   Found " + str(len(layouts)) + " layouts beginning 'zeroheight_'"
        except:
            pass


        print utils.COLOR_CODES.UNDERLINE + "Searching Screen Previews" + utils.COLOR_CODES.ENDC

        previews = []

        try:
            for filestr in os.listdir("./zeroheight_screen_previews"):
                previews.append(filestr)
        except:
            pass

        print "   Found " + str(len(previews)) + " screen previews"

        hasHistory = False

        print utils.COLOR_CODES.UNDERLINE + "Searching for history" + utils.COLOR_CODES.ENDC
        if os.path.isfile('.zhhistory'):
            hasHistory = True
            print "   Found .zhhistory file"
        else:
            print "   Found no .zhhistory file"

        if colors > 0 or strings > 0 or len(drawables) > 0 or len(layouts) > 0 \
            or hasHistory or len(previews) > 0:

            delete = False
            if force:
                delete = True
            else:
                delete = utils.yesno(utils.COLOR_CODES.WARNING + "Delete these resources" +\
                    utils.COLOR_CODES.ENDC)

            if delete:
                if colors > 0:
                    print utils.COLOR_CODES.FAIL + "Removing colors" + utils.COLOR_CODES.ENDC
                    self.__color_manager.remove_elements_with_prefix("zh_")
                    self.__color_manager.write_xml(self.__asset_folder)
                if strings > 0:
                    print utils.COLOR_CODES.FAIL + "Removing strings" + utils.COLOR_CODES.ENDC
                    self.__string_manager.remove_elements_with_prefix("zh_")
                    self.__string_manager.write_xml(self.__asset_folder)


                if len(drawables) > 0:
                    print utils.COLOR_CODES.FAIL + "Removing drawables" + utils.COLOR_CODES.ENDC
                    resolutions = ["mdpi","hdpi","xhdpi","xxhdpi","xxxhdpi"]

                    for drawable in drawables:
                        for resolution in resolutions:
                            res_dir = os.path.join(self.__asset_folder,"drawable-"+resolution,\
                                drawable)
                            try:
                                os.remove(res_dir)
                            except OSError:
                                try:
                                    res_dir = os.path.join(self.__asset_folder,\
                                        "mipmap-"+resolution,drawable)
                                    os.remove(res_dir)
                                except OSError:
                                    pass

                if len(layouts) > 0:
                    print utils.COLOR_CODES.FAIL + "Removing layouts" + utils.COLOR_CODES.ENDC
                    for layout in layouts:
                        layout_file = os.path.join(self.__asset_folder,"layout",layout)                        
                        try:
                            os.remove(layout_file)
                        except OSError:
                            pass

                if len(previews) > 0:
                    print utils.COLOR_CODES.FAIL + "Removing previews" + utils.COLOR_CODES.ENDC
                    shutil.rmtree("./zeroheight_screen_previews")

                if hasHistory:
                    print utils.COLOR_CODES.FAIL + "Removing .zhhistory" + utils.COLOR_CODES.ENDC
                    try:
                        os.remove(".zhhistory")
                    except OSError:
                        pass
        else:
            print "You're already clean :]"
