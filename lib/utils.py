#!/usr/bin/env python 
import os, traceback, urllib2, json, sys, re, getpass, errno
from urllib2 import Request, urlopen, URLError

CONFIG_NAME = ".zhconfig"

PROJECT_TYPES = { 
    'IOS' : 'ios',
    'ANDROID' : 'android'
}

if os.environ.get("ZH_DEBUG"):
    API_HOST = "localhost:3000"
else:
    API_HOST = "beta.zeroheight.com"

def androidify_name(name):
    new = re.sub('[^0-9a-zA-Z]+', '_', name.lower())
    if new[0].isdigit():
        new = '_'+new
    return new

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

def file_to_json(path):
    file_json = {}
    if (os.path.isfile(path)):
        f = open(path, "r").read()
        file_json = json.loads(f)
    return file_json

def set_json_file_kv(path, key, value):
    file_json = file_to_json(path)
    file_json[key] = value
    updated_json = json.dumps(file_json, sort_keys=True,
        indent=4, separators=(',', ': '))
    f = open(path, "w")
    f.write(updated_json)

def get_json_file_kv(path, key):
    file_json = file_to_json(path)
    return file_json.get(key)

class COLOR_CODES:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def uncaught_exception(exctype, value, tb):
    trace = ""
    if tb:
        trace = traceback.format_tb(tb)
    else:
        trace = traceback.format_exc()

    error_message = str(value) + "\n" + str(trace)
    send_error(error_message)

def send_error(error_message, fatal=True, api_token=None):
    api_token = api_token or "kH1AxT3NJz7Cxc" # default error-only token
    
    if os.environ.get("ZH_DEBUG"):
        print "DEBUG PRINT ERROR : " + error_message

    postdata = {
        "source" : "zh-cli",
        "client_uid" : getpass.getuser(),
        "errorMessage" : error_message
    }
        
    url = "http://"+API_HOST+"/api/errors"
    req = urllib2.Request(url)
    req.add_header('Content-Type','application/json')
    req.add_header("Authorization", "Token token=\"" + api_token + "\"")
    data = json.dumps(postdata)
    # don't really care about the response, we tried our best!
    urllib2.urlopen(req, data)

    if (fatal):
        print COLOR_CODES.FAIL + "Something went wrong, we're looking into it. Feel free to get in touch at support@zeroheight.com." + COLOR_CODES.ENDC  
        sys.exit(1)

def error(e, fatal=True):
    error_message = type(e).__name__ + " " + str(e) + "\n" + traceback.format_exc()
    send_error(error_message, fatal)

def api_get_request(url, callback, api_token):
    try:
        req = urllib2.Request(url)
        req.add_header("Authorization", "Token token=\"" + api_token + "\"")
        response = urllib2.urlopen(req)
        if response.getcode() == 200:
            callback(response)
        else:
            error("Unexpected response code from request to " +
                    url + " = " + response.getcode())
    except Exception as e:
        if hasattr(e, 'code'):
            if e.code == 401:
                print COLOR_CODES.FAIL + "No valid API token found. Get the token from a designer and run zh auth \"token\"" + COLOR_CODES.ENDC
                sys.exit(1)
            elif e.code == 404:
                msg = COLOR_CODES.FAIL + url + " not found, please try uploading resource again." +COLOR_CODES.ENDC
                error(msg, False)
            else:
                error(e)    
        else:
            error(e)

def api_post_request(url, postdata, callback, api_token):
    try:
        req = urllib2.Request(url)
        req.add_header("Authorization", "Token token=\"" + api_token + "\"")
        req.add_header('Content-Type','application/json')
        data = json.dumps(postdata)
        response = urllib2.urlopen(req, data)
        if response.getcode() == 200:
            body = response.read().decode('utf-8')
            data = json.loads(body)
            callback(data)
        else:
            error("Unexpected response code from request to " +
                    url + " = " + response.getcode())
    except Exception as e:
        if hasattr(e, 'code'):
            if e.code == 401:
                print COLOR_CODES.FAIL + "No valid API token found. Get the token from a designer and run zh auth \"token\"" + COLOR_CODES.ENDC
                sys.exit(1)
            elif e.code == 404:
                msg = COLOR_CODES.FAIL + url + " not found, please try uploading resource again." +COLOR_CODES.ENDC
                error(msg, False)
            else:
                error(e)    
        else:
            error(e)

