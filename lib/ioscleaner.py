import os,re,utils,shutil

class iOSCleaner(object):
    def __init__(self,asset_folder):
        self.__asset_folder = asset_folder

    def clean(self,force):

        assets = []
        screens = []
        asset_re = re.compile("^.*_\d+\.imageset$")

        for filestr in os.listdir(self.__asset_folder):

            if asset_re.match(filestr) is not None:
                assets.append(filestr)
            else:
                if filestr == "Shared":
                    assets.append(filestr)
                    print "Found 'Shared' asset set"
                elif filestr != ".DS_Store":
                    # peek inside directory, if there's a screen preview count this as a zh imageset
                    thatdir = os.path.join(self.__asset_folder,filestr)
                    try:
                        contents = os.listdir(thatdir)
                        for downdir in contents:
                            if (downdir == "buttons") or (downdir == "icons") or (downdir == "images"):
                                assets.append(filestr)
                                print "Found '" + filestr + "' asset set"
                                break
                    except:
                        pass

        print utils.COLOR_CODES.OKGREEN + "Found " + str(len(assets)) +\
            " zeroheight screen asset sets" + utils.COLOR_CODES.ENDC

        hasHistory = False
        if os.path.isfile('.zhhistory'):
            hasHistory = True
            print utils.COLOR_CODES.OKGREEN + "Found .zhhistory file" + utils.COLOR_CODES.ENDC

        if len(assets) > 0 or len(screens) > 0 or hasHistory:

            delete = False
            if force:
                delete = True
            else:
                delete = utils.yesno(utils.COLOR_CODES.WARNING + "Delete these resources" +\
                    utils.COLOR_CODES.ENDC)

            if delete:
                if len(assets) > 0:
                    print utils.COLOR_CODES.FAIL + "Removing asset imagesets" + utils.COLOR_CODES.ENDC
                    for asset in assets:
                        imageset = os.path.join(self.__asset_folder,asset)
                        shutil.rmtree(imageset)

                if hasHistory:
                    print utils.COLOR_CODES.FAIL + "Removing .zhhistory" + utils.COLOR_CODES.ENDC
                    try:
                        os.remove(".zhhistory")
                    except OSError:
                        pass
        else:
            print "You're already clean :]"                        
