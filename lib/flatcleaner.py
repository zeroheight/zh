import os,re,utils,shutil

class FlatCleaner(object):
    def __init__(self,asset_folder):
        self.__asset_folder = asset_folder

    def clean(self,force):
        dir_count = 0

        for filestr in os.listdir(self.__asset_folder):
            if os.path.isdir(filestr):    
                shutil.rmtree(filestr)
                dir_count += 1

        print utils.COLOR_CODES.FAIL + "Deleted " + str(dir_count) + " top level directories"
        try:
            os.remove(".zhhistory")
        except OSError:
            pass