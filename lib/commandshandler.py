#!/usr/bin/env python 
import json,sys,calendar,time

import utils, apitoken as API_TOKEN, projectconfig, \
    androidpullhandler, iospullhandler, changeshandler, androidcleaner, ioscleaner, flatpullhandler, \
    flatcleaner

class CommandsHandler:

    def resolve_pullhandler(self):
        asset_folder, proj_type, file_type = projectconfig.resolve_project_details()
        if asset_folder:
            if proj_type == utils.PROJECT_TYPES['FLAT']:
                self.__pullhandler = flatpullhandler.FlatPullHandler(asset_folder,file_type,proj_type)
            if proj_type == utils.PROJECT_TYPES['ANDROID']:
                self.__pullhandler = androidpullhandler.AndroidPullHandler(asset_folder,file_type,proj_type)
            if proj_type == utils.PROJECT_TYPES['IOS']:
                self.__pullhandler = iospullhandler.iOSPullHandler(asset_folder,file_type,proj_type)

    def resolve_cleaner(self):
        asset_folder, proj_type, file_type = projectconfig.resolve_project_details()
        if asset_folder:
            if proj_type == utils.PROJECT_TYPES['FLAT']:
                self.__cleaner = flatcleaner.FlatCleaner(asset_folder)
            if proj_type == utils.PROJECT_TYPES['ANDROID']:
                self.__cleaner = androidcleaner.AndroidCleaner(asset_folder)
            if proj_type == utils.PROJECT_TYPES['IOS']:
                self.__cleaner = ioscleaner.iOSCleaner(asset_folder)

    #### ZH CHANGES
    def changes(self,args):
        self.resolve_pullhandler()
        API_TOKEN.resolve(args.changeset_id)
        ch = changeshandler.ChangesHandler(self.__pullhandler)
        ch.handle_changeset(args.changeset_id)

    #### ZH CLEAN
    def clean(self,args):
        self.resolve_cleaner()
        self.__cleaner.clean(args.force)

    ##### ZH PULL
    def pull(self,args):        
        pull_id = args.pull_id
        if not pull_id:            
            pull_id = projectconfig.get_project_id()
            if not pull_id:
                print utils.COLOR_CODES.FAIL +"Please provide a project ID "
                print "e.g. zh pull pxzr7sagugxdzo7h80uq"+ utils.COLOR_CODES.ENDC
                sys.exit(1)

        self.resolve_pullhandler()
        API_TOKEN.resolve(pull_id)
        projectconfig.set_project_id(pull_id)
        last_pull_time = projectconfig.get_last_pull_time()
        self.__pullhandler.pull(pull_id,args,last_pull_time)
