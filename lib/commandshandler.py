#!/usr/bin/env python 
import json,sys
import utils, apitoken as API_TOKEN, projectconfig, \
    androidpullhandler, iospullhandler, changeshandler


class CommandsHandler:
    def __init__(self):
        asset_folder, proj_type, file_type = projectconfig.resolve_project_details()
        if asset_folder:
            if proj_type == utils.PROJECT_TYPES['ANDROID']:
                self.__pullhandler = androidpullhandler.AndroidPullHandler(asset_folder,file_type)
            if proj_type == utils.PROJECT_TYPES['IOS']:
                self.__pullhandler = iospullhandler.iOSPullHandler(asset_folder,file_type)

    #### ZH CHANGES
    def changes(self,args):
        API_TOKEN.resolve(args.changeset_id)
        ch = changeshandler.ChangesHandler(self.__pullhandler)
        ch.handle_changeset(args.changeset_id)

    ##### ZH PULL
    def pull(self,args):
        pull_id = args.pull_id
        if not pull_id:
            pull_id = projectconfig.get_project_id()
            if not pull_id:
                print utils.COLOR_CODES.FAIL +"Please provide a project ID"+ utils.COLOR_CODES.ENDC
                sys.exit(1)
        API_TOKEN.resolve(pull_id)
        projectconfig.set_project_id(pull_id)
        self.__pullhandler.pull(pull_id)