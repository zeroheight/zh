#!/usr/bin/env python 
import urllib, urllib2, sys, json, getpass, os, cgi, traceback, time, argparse, re, operator
from urllib2 import Request, urlopen, URLError

import projectconfig, utils, apitoken as API_TOKEN, pullhandler
import thirdparty.webcolors as webcolors
from subprocess import call
from xml.sax.saxutils import escape

class iOSPullHandler(pullhandler.PullHandler):

    def __init__(self,asset_folder,file_type,proj_type):

        super(iOSPullHandler, self).__init__(asset_folder,file_type,proj_type)
        self.expected_resolutions = ["1"]

        self.display_resolutions = {
            "1"    : "1x",
            "2"    : "2x",
            "3"    : "3x",
            "svg"  : "svg",
            "pdf"  : "pdf"
        }

        self.suffix = "x"

        self.screen_layouts = {}

    def get_asset_name(self,tag):
        suffix = ""
        if self.config["add-id-suffix"]:
            suffix = "_"+str(tag['id'])
        return tag['name']+suffix

    def write_asset_file(self,screen_name,asset_name,\
        tag,response,extension,useRetina4Catalogue,shared_asset):

        base = os.path.abspath(self.asset_folder)

        screen_dir = ("Shared" if shared_asset else screen_name)

        type_dir = ""
        if self.config["use-category-folders"]:
            type_dir = (tag['category']+'s') if tag['category'] else "images"

        full_dir = os.path.join(base, screen_dir, type_dir, asset_name+".imageset")

        filename = asset_name +"-4x" + extension
        four_path = os.path.join(full_dir, filename)
        
        if not os.path.exists(full_dir):
            utils.mkdir_p(full_dir)

        with open(four_path, "wb") as local_file:
            local_file.write(response.read())

        resolutions = ["1","2","3"]
        for res in resolutions:
            filename = asset_name +"@" + res + "x" + extension
            full = os.path.join(full_dir, filename)

            factor = (float(res) / 4) * 100
            call([utils.CONVERT_BIN,four_path,
                "-alpha","on","-background","transparent",
                "-channel","RGBA"
                ,"-resize",str(factor)+"%",full])

            if self.config['optimise-assets']:
                utils.png_optimise(full)

            self.__write_asset_catalog_contents(full_dir, filename, res, useRetina4Catalogue)

        # clean up 4x resolution as not used in iOS (TODO: fetche 3x resolution instead for iOS?)
        os.remove(four_path)


    def should_fetch_resolution(self,resolution):
        return (resolution == "1" or resolution == "2" or resolution == "3" or resolution == "2r4")

    def add_button_layout(self,screen,element):
        pass

    def add_text_layout(self,screen,element):
        # layout not yet supported for iOS
        return

    def add_button_layout(self,screen,tag,bgShape=None):
        # layout not yet supported for iOS
        return

    def add_text_layout(self,screen,tag):
        # layout not yet supported for iOS
        return

    def add_image_layout(self,screen,tag):
        # layout not yet supported for iOS
        return

    def try_add_shape(self,screen,tag):
        # layout not yet supported for iOS
        return

    def begin_pull(self):
        pass

    def finalise_pull(self):
        pass

    def finalise_screen(self,screen):
        # layout not yet supported for iOS
        return

    def __write_asset_catalog_contents(self,dir, filename, resolution=None, useRetina4Catalogue=False):
        path = os.path.join(dir, "Contents.json")

        contents = {}

        idiom = "universal"

        if useRetina4Catalogue:
            idiom = "iphone"

        if not resolution or resolution == "svg" or resolution == "pdf":
            contents = {
                "images" : [
                    {
                        "idiom" : idiom,
                        "filename" : filename
                    }
                ],
                "info" : {
                    "version" : 1,
                    "author" : "xcode"
                }
            }
        elif os.path.exists(path):
            # add to existing contents file
            with open(path) as local_file:    
                contents = json.load(local_file)

            found_resolution = False        

            for image in contents["images"]:
                if image["idiom"] != idiom:
                    image["idiom"] = idiom

                if resolution == "2r4":
                    if "subtype" in image and image["subtype"] == "retina4":
                        found_resolution = True
                        image["filename"] = filename
                else:
                    if image["scale"] == resolution+"x":
                        if "subtype" in image and image["subtype"] == "retina4":
                            # don't count 2r4 as 2x
                            continue
                        else:                    
                            found_resolution = True
                            image["filename"] = filename

            subtype = ""

            if resolution == "2r4":
                subtype = "retina4"
                resolution = "2"

            if not found_resolution:
                contents["images"].append({
                        "idiom" : idiom,
                        "scale" : resolution+"x",
                        "filename" : filename
                })       
        else:

            subtype = ""

            if resolution == "2r4":
                subtype = "retina4"
                resolution = "2"

            contents = {
                "images" : [
                    {
                        "idiom"    : idiom,
                        "scale"    : resolution+"x",
                        "filename" : filename
                    }
                ],
                "info" : {
                    "version" : 1,
                    "author" : "xcode"
                }
            }

        contents_json = json.dumps(contents, sort_keys=True,
            indent=4, separators=(',', ': '))

        with open(path, "wb") as local_file:
            local_file.write(contents_json)
