#!/usr/bin/env python 
import urllib, urllib2, sys, json, getpass, os, cgi, traceback, dateutil.parser, time, argparse, re, operator
from urllib2 import Request, urlopen, URLError

import projectconfig, utils, apitoken as API_TOKEN, pullhandler
import thirdparty.webcolors as webcolors
from xml.sax.saxutils import escape

class iOSPullHandler(pullhandler.PullHandler):

    def __init__(self):
        self.expected_resolutions = ["1"]

        self.display_resolutions = {
            "1"    : "1x",
            "2"    : "2x",
            "3"    : "3x",
            "svg"  : "svg",
            "pdf"  : "pdf"
        }

        self.suffix = "x"

        self.screen_layouts = {}

    def write_screen_preview(self,screen_name,response,asset_folder):
        base = os.path.abspath(asset_folder)
        full_dir = os.path.join(base, screen_name, "Preview.imageset")
        filename = "preview.png"
        full = os.path.join(full_dir, filename)
        
        if not os.path.exists(full_dir):
            utils.mkdir_p(full_dir)

        # TODO: cater for asset catalog Preview and preview.png name clashes
        with open(full, "wb") as local_file:
            local_file.write(response.read())

        self.__write_asset_catalog_contents(full_dir, filename)

    def write_asset_file(self,screen_name,asset_name,\
        resolution,response,extension,useRetina4Catalogue,asset_folder):
        filename = asset_name +"-" +resolution + "x" + extension

        if resolution == "pdf" or resolution == "svg":
            filename = asset_name + extension

        base = os.path.abspath(asset_folder)
        full_dir = os.path.join(base, screen_name, asset_name+".imageset")
        full = os.path.join(full_dir, filename)
        
        if not os.path.exists(full_dir):
            utils.mkdir_p(full_dir)

        with open(full, "wb") as local_file:
            local_file.write(response.read())

        self.__write_asset_catalog_contents(full_dir, filename, resolution, useRetina4Catalogue)

    def should_fetch_resolution(self,resolution):
        return (resolution == "1" or resolution == "2" or resolution == "3" or resolution == "2r4")

    def add_button_layout(self,screen,element):
        pass

    def add_text_layout(self,screen,element):

        styles = json.loads(element['styles'])

        rgb = webcolors.hex_to_rgb(webcolors.normalize_hex(styles['color']))

        textFactor = int(screen['designed_at'])
        if "pt" in styles["fontSize"]:
            textFactor *= 0.53

        xml =   ("""
                <label opaque="NO" userInteractionEnabled="NO" contentMode="left" horizontalHuggingPriority="251" verticalHuggingPriority="251" fixedFrame="YES" text="%s" lineBreakMode="tailTruncation" baselineAdjustment="alignBaselines" adjustsFontSizeToFit="NO" translatesAutoresizingMaskIntoConstraints="NO" id="%s">
                    <rect key="frame" x="%d" y="%d" width="%d" height="%d"/>
                    <fontDescription key="fontDescription" name="Futura-Medium" family="Futura" pointSize="%d"/>
                    <color key="textColor" red="%f" green="%f" blue="%f" alpha="1" colorSpace="calibratedRGB"/>
                    <nil key="highlightedColor"/>
                </label>""" % (\
                escape(styles['text']),\
                escape(element['uid']),\
                element['x_pos'] / int(screen['designed_at']),\
                element['y_pos'] / int(screen['designed_at']),\
                element['width'] / int(screen['designed_at']),\
                element['height']/ int(screen['designed_at']),\
                int(re.sub("[^0-9.]", "",styles["fontSize"])) / textFactor,\
                (rgb[0] / 255.0),\
                (rgb[1] / 255.0),\
                (rgb[2] / 255.0)))
    
        self.layout_details [element['id']] = {"xml" : xml}
        self.layout_ordering[element['id']] = element['z_pos']


    def add_image_layout(self,screen,element):

        image_name = escape(element['name']+"_"+element['uid'])

        xml =   ("""
                <imageView userInteractionEnabled="NO" contentMode="scaleToFill" horizontalHuggingPriority="251" verticalHuggingPriority="251" fixedFrame="YES" image="%s" translatesAutoresizingMaskIntoConstraints="NO" id="%s">
                    <rect key="frame" x="%d" y="%d" width="%d" height="%d"/>
                </imageView>""" % (\
                image_name,\
                escape(element['uid']),\
                element['x_pos'] / int(screen['designed_at']),\
                element['y_pos'] / int(screen['designed_at']),\
                element['width'] / int(screen['designed_at']),\
                element['height']/ int(screen['designed_at'])))

        resource = """<image name="%s" width="%d" height="%d"/>""" % (image_name,element['width'],element['height'])

        self.layout_details [element['id']] = {"xml" : xml, "resource" : resource}
        self.layout_ordering[element['id']] = element['z_pos']

    def begin_pull(self,asset_folder):
        pass

    def finalise_pull(self,asset_folder):
        pass

    def finalise_screen(self,screen,asset_folder):
        full_dir = os.path.join(asset_folder, "../zeroheight-layouts")

        if not os.path.exists(full_dir):
            utils.mkdir_p(full_dir)

        path = os.path.join(full_dir, screen['name'] + ".xib")

        print "Writing layout XMLs to " + str(full_dir)

        with open(path, 'w') as f:
            f.write(("""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<document type="com.apple.InterfaceBuilder3.CocoaTouch.XIB" version="3.0" toolsVersion="7706" systemVersion="14D113c" targetRuntime="iOS.CocoaTouch" propertyAccessControl="none" useAutolayout="YES" useTraitCollections="YES">
    <dependencies>
        <plugIn identifier="com.apple.InterfaceBuilder.IBCocoaTouchPlugin" version="7703"/>
    </dependencies>
    <objects>
        <placeholder placeholderIdentifier="IBFilesOwner" id="-1" userLabel="File's Owner"/>
        <placeholder placeholderIdentifier="IBFirstResponder" id="-2" customClass="UIResponder"/>
        <view contentMode="scaleToFill" id="%s">
            <rect key="frame" x="0.0" y="0.0" width="375" height="667"/>
            <autoresizingMask key="autoresizingMask" widthSizable="YES" heightSizable="YES"/>
            <subviews>""" % (screen['id'])).encode('utf-8'))

            ordered_layouts = sorted(self.layout_ordering.items(), 
                key=operator.itemgetter(1), 
                reverse=True)

            resources = []

            for pair in ordered_layouts:
                element_id = pair[0]
                if element_id in self.layout_details:
                    xml = self.layout_details[element_id]["xml"]
                    if "resource" in self.layout_details[element_id]:
                        resources.append(self.layout_details[element_id]["resource"])
                    f.write(xml.encode('utf-8'))

            f.write(("""
            </subviews>
            <color key="backgroundColor" white="1" alpha="1" colorSpace="custom" customColorSpace="calibratedWhite"/>
            <simulatedScreenMetrics key="simulatedDestinationMetrics" type="retina47"/>
            <point key="canvasLocation" x="363" y="403"/>
        </view>
    </objects>
    <resources>""").encode('utf-8'))

            for res in resources:
                f.write(("""
        %s""" % res).encode('utf-8'))

            f.write(("""
    </resources>
</document>""").encode('utf-8'))

    def __write_asset_catalog_contents(self,dir, filename, resolution=None, useRetina4Catalogue=False):
        path = os.path.join(dir, "Contents.json")

        contents = {}

        idiom = "universal"

        if useRetina4Catalogue:
            idiom = "iphone"

        if not resolution or resolution == "svg" or resolution == "pdf":
            contents = {
                "images" : [
                    {
                        "idiom" : idiom,
                        "filename" : filename
                    }
                ],
                "info" : {
                    "version" : 1,
                    "author" : "xcode"
                }
            }
        elif os.path.exists(path):
            # add to existing contents file
            with open(path) as local_file:    
                contents = json.load(local_file)

            found_resolution = False        

            for image in contents["images"]:
                if image["idiom"] != idiom:
                    image["idiom"] = idiom

                if resolution == "2r4":
                    if "subtype" in image and image["subtype"] == "retina4":
                        found_resolution = True
                        image["filename"] = filename
                else:
                    if image["scale"] == resolution+"x":
                        if "subtype" in image and image["subtype"] == "retina4":
                            # don't count 2r4 as 2x
                            continue
                        else:                    
                            found_resolution = True
                            image["filename"] = filename

            subtype = ""

            if resolution == "2r4":
                subtype = "retina4"
                resolution = "2"

            if not found_resolution:
                contents["images"].append({
                        "idiom" : idiom,
                        "scale" : resolution+"x",
                        "filename" : filename,
                        "subtype" : subtype
                })       
        else:

            subtype = ""

            if resolution == "2r4":
                subtype = "retina4"
                resolution = "2"

            contents = {
                "images" : [
                    {
                        "idiom"    : idiom,
                        "scale"    : resolution+"x",
                        "filename" : filename,
                        "subtype"  : subtype
                    }
                ],
                "info" : {
                    "version" : 1,
                    "author" : "xcode"
                }
            }

        contents_json = json.dumps(contents, sort_keys=True,
            indent=4, separators=(',', ': '))

        with open(path, "wb") as local_file:
            local_file.write(contents_json)


# def handle_screen_widgets(self,screen_name,widgets):
#         if not screen_name in self.screen_layouts:
#             self.screen_layouts[screen_name] = []

#         screen_layout = self.screen_layouts[screen_name]

#         for w in widgets:
#             name = w["name"]

#             if w["type"] == "text" :
#                 styles = json.loads(w["styles"])
#                 bounds = json.loads(styles["bounds"])

#                 size = "0"
#                 if "size" in styles:
#                     size = styles["size"]

#                 color = styles["color"]

#                 screen_layout.append({
#                     "type"      : "text",
#                     "name"      : name,
#                     "bounds"    : bounds,
#                     "id"        : w["id"],
#                     "text"      : styles["text"],
#                     "typeface"  : styles["font"],
#                     "textStyle" : styles["style"],
#                     "textSize"  : size,
#                     "textColor" : color
#                 })

#             if w["type"] == "image" :
#                 styles = json.loads(w["styles"])
#                 bounds = json.loads(styles["bounds"])

#                 screen_layout.append({
#                     "type"      : "image",
#                     "id"        : w["id"],
#                     "name"      : name,
#                     "bounds"    : bounds
#                 })

#     def finalise(self,asset_folder):
#         if self.do_layouts or self.all_args:
#             self.__write_layout_xmls(asset_folder)

#     def __write_layout_xmls(self,asset_folder):
#         full_dir = os.path.join(".", "test")

#         if not os.path.exists(full_dir):
#             utils.mkdir_p(full_dir)

#         print "Writing layout XMLs to " + str(full_dir)

#         for screen_name, elements in self.screen_layouts.iteritems():

#             path = os.path.join(full_dir, screen_name.lower() + ".xib")

#             with open(path, 'w') as f:
#                 f.write(("""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
# <document type="com.apple.InterfaceBuilder3.CocoaTouch.XIB" version="3.0" toolsVersion="7706" systemVersion="14D113c" targetRuntime="iOS.CocoaTouch" propertyAccessControl="none" useAutolayout="YES" launchScreen="YES" useTraitCollections="YES">
#     <dependencies>
#         <plugIn identifier="com.apple.InterfaceBuilder.IBCocoaTouchPlugin" version="7703"/>
#     </dependencies>
#     <objects>
#         <placeholder placeholderIdentifier="IBFilesOwner" id="-1" userLabel="File's Owner"/>
#         <placeholder placeholderIdentifier="IBFirstResponder" id="-2" customClass="UIResponder"/>
#         <view contentMode="scaleToFill" id="iN0-l3-epB">
#             <rect key="frame" x="0.0" y="0.0" width="384" height="678"/>
#             <autoresizingMask key="autoresizingMask" widthSizable="YES" heightSizable="YES"/>
#             <subviews>""").encode("utf-8"))

#                 for element in elements:
#                     if element["type"] == "text":

#                         # color = element["textColor"]
#                         # if color.lower() in self.color_to_name_id:
#                         #     color = "@color/" + self.color_to_name_id[color.lower()]

#                         f.write(("""
# <label opaque="NO" userInteractionEnabled="NO" contentMode="left" horizontalHuggingPriority="251" verticalHuggingPriority="251" fixedFrame="YES" text="%s" lineBreakMode="tailTruncation" baselineAdjustment="alignBaselines" adjustsFontSizeToFit="NO" translatesAutoresizingMaskIntoConstraints="NO" id="%d">
#     <rect key="frame" x="%d" y="%d" width="%d" height="%d"/>
#     <fontDescription key="fontDescription" name="Arial" family="Arial" pointSize="%d"/>
#     <color key="textColor" red="0.0" green="0.0" blue="0.0" alpha="1" colorSpace="calibratedRGB"/>
#     <nil key="highlightedColor"/>
# </label>
# """ %
#             (element["text"].replace('&', '&amp;'),\
#                 element["id"],\
#                 int(element["bounds"]["left"]) / 2,\
#                 int(element["bounds"]["top"]) / 2,\
#                 (int(element["bounds"]["right"]) - int(element["bounds"]["left"])) / 2,\
#                 (int(element["bounds"]["bottom"]) - int(element["bounds"]["top"])) / 2,\
#                 int(re.sub("[^0-9]", "",element["textSize"])) / 3)).encode('utf-8'))

#                     if element["type"] == "image":
#                         f.write(("""
# <imageView opaque="NO" clipsSubviews="YES" multipleTouchEnabled="YES" contentMode="scaleAspectFit" fixedFrame="YES" image="%s" translatesAutoresizingMaskIntoConstraints="NO" id="%d">
#     <rect key="frame" x="%d" y="%d" width="%d" height="%d"/>
# </imageView>\n""" % (\
#             element["name"],\
#             element["id"],\
#             int(element["bounds"]["left"]) / 2,\
#             int(element["bounds"]["top"]) / 2,\
#             (int(element["bounds"]["right"]) - int(element["bounds"]["left"])) / 2,\
#             (int(element["bounds"]["bottom"]) - int(element["bounds"]["top"])) / 2)).encode('utf-8'))

#                 f.write(("""</subviews>
#             <color key="backgroundColor" white="1" alpha="1" colorSpace="custom" customColorSpace="calibratedWhite"/>
#             <nil key="simulatedStatusBarMetrics"/>
#             <freeformSimulatedSizeMetrics key="simulatedDestinationMetrics"/>
#             <variation key="default">
#                 <mask key="subviews">
#                     <exclude reference="GB3-7Z-7QO"/>
#                     <exclude reference="0LA-sJ-lVg"/>
#                     <exclude reference="iU1-CY-TSe"/>
#                 </mask>
#             </variation>
#             <point key="canvasLocation" x="500" y="554"/>
#         </view>
#     </objects>
#     <resources>
#         <image name="Avatar 1" width="50" height="50"/>
#         <image name="Preview" width="758" height="1335"/>
#         <image name="Rectangle 1" width="75" height="99"/>
#     </resources>
# </document>
# """.encode('utf-8')))

