#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import urllib, urllib2, sys, json, getpass, os, cgi, traceback, dateutil.parser, \
    time, argparse, re, operator, base64, math
from urllib2 import Request, urlopen, URLError
from subprocess import call
import xml.etree.ElementTree as ET

import androidxmls

import projectconfig, utils, apitoken as API_TOKEN, pullhandler
import thirdparty.webcolors as webcolors
import thirdparty.xmlpp as xmlpp

class AndroidPullHandler(pullhandler.PullHandler):

    def __init__(self,asset_folder,file_type):

        super(AndroidPullHandler, self).__init__(asset_folder,file_type)
        
        self.expected_resolutions = ["1"]

        self.display_resolutions = {
            "0.75" : "ldpi",
            "1"    : "mdpi",
            "1.5"  : "hdpi",
            "2"    : "xhdpi",
            "3"    : "xxhdpi",
            "4"    : "xxxhdpi",
            "svg"  : "svg",
            "pdf"  : "pdf"
        }

        self.previous_ids = set([])

        self.color_manager  = androidxmls.AndroidColors()
        self.string_manager = androidxmls.AndroidStrings()
   
    def write_screen_preview(self,screen,response):
        _, params = cgi.parse_header(response.headers.get('Content-Disposition', ''))
        extension = os.path.splitext(str(params["filename"]))[-1]

        full_dir = os.path.join(".", "zeroheight_screen_previews")
        filename = utils.androidify_name(screen["name"]) + extension
        full = os.path.join(full_dir, filename)

        print "    Writing screen preview to " + full

        if not os.path.exists(full_dir):
            utils.mkdir_p(full_dir)

        with open(full, "wb") as local_file:
            local_file.write(response.read())

        utils.mkdir_p("./.zhthumbnails")
        path = "./.zhthumbnails/temp_thumbnail.png"
        call(["convert",full,"-resize","x100",path])
        with open(path, "rb") as image_file:
            image = base64.b64encode(image_file.read())
        screen["preview"] = image

    def write_asset_file(self,screen_name,asset_name,tag,response,extension,useRetina4Catalogue):
            
        full_dir = os.path.join(self.asset_folder,"drawable-"+self.display_resolutions["4"])

        filename = "ic_" + utils.androidify_name(asset_name) + extension
        fourPath = os.path.join(full_dir, filename)

        if not os.path.exists(full_dir):
            utils.mkdir_p(full_dir)

        with open(fourPath, "wb") as local_file:
            local_file.write(response.read())

        other_resolutions = ["1","1.5","2","3"]
        for res in other_resolutions:
            factor = (float(res) / 4) * 100
            full_dir = os.path.join(self.asset_folder,"drawable-"+self.display_resolutions[res])
            if not os.path.exists(full_dir):
                utils.mkdir_p(full_dir)
            path = os.path.join(full_dir, filename)
            call(["convert",fourPath,"-resize",str(factor)+"%",path])

        utils.mkdir_p("./.zhthumbnails")
        path = "./.zhthumbnails/temp_thumbnail.png"
        call(["convert",fourPath,"-resize","x100",path])
        with open(path, "rb") as image_file:
            image = base64.b64encode(image_file.read())
        tag["image"] = image

    def try_add_shape(self,screen,tag):
        fail = None

        if not tag["styles"]:
            return fail

        try:

            root = ET.fromstring(tag["styles"])

            height = root.get("height")
            width = root.get("width")

            defs = root.find("{http://www.w3.org/2000/svg}defs")

            # we don't support complicated definitions!
            if defs is not None:
                if len(defs) > 0:
                    return fail

            page = root.find("{http://www.w3.org/2000/svg}g")
            artboard = page[0]

            # traverse the groups until we hit shapes
            parent = artboard
            child = parent[0]

            while(child.tag == "{http://www.w3.org/2000/svg}g"):
                parent = child
                child = parent[0]

            shapes = list(parent)

            # we only support one simple shape for now
            if len(shapes) > 1:
                return fail

            shape = shapes[0]
            # skip the namespace to get the shape type
            shape_type = shape.tag[28:]

            allowed_shape = None

            # we only support a few shapes for now
            if shape_type == "rect":
                allowed_shape = "rectangle"

            if shape_type == "ellipse":
                allowed_shape = "oval"

            if shape_type == "path":
                d = shape.get("d")
                coords = re.search("M([\d.]*)\,([\d.]*) L([\d.]*)\,([\d.]*)",d)
                x1 = coords.group(1)
                y1 = coords.group(2)
                x2 = coords.group(3)
                y2 = coords.group(4)

                if y1 == y2:
                    allowed_shape = "line"

            if allowed_shape is not None:
                bordercolor = artboard.get("stroke")
                borderwidth = artboard.get("stroke-width")
                fill = artboard.get("fill")
                radius = shape.get("rx")
                opacity = artboard.get("opacity")

                if opacity is not None:
                    if bordercolor is not None:
                        bordercolor = "#{0:0{1}x}{2}".format(int(255 * float(opacity)),2,bordercolor[1:])
                    if fill is not None:
                        fill = "#{0:0{1}x}{2}".format(int(255 * float(opacity)),2,fill[1:])

                return self.__write_shape_xml(screen,tag,allowed_shape,width,height,
                    bordercolor,borderwidth,fill,radius)
        except:
            if os.environ.get("ZH_DEBUG"):
                print "DEBUG: Failed to make shape for " + tag["name"] + " " + screen["name"]            
            return fail
        
        return fail

    def __write_shape_xml(self,screen,tag,shape_type,width,height,bordercolor,borderwidth,fill,radius):
        full_dir = os.path.join(self.asset_folder, "drawable")

        if not os.path.exists(full_dir):
            utils.mkdir_p(full_dir)

        name = utils.androidify_name(shape_type+"_" + tag['name']+"_"+str(tag["id"]))
        path = os.path.join(full_dir, name + ".xml")

        px_to_dp_factor = self.__get_px_to_dp_factor(screen)

        with open(path, 'w') as f:
            f.write(("""<?xml version="1.0" encoding="utf-8"?>
<shape
  xmlns:android="http://schemas.android.com/apk/res/android"
  android:shape="%s">""" % shape_type
    ).encode('utf-8'))

            borderwidth = (borderwidth if borderwidth else "1")
            borderwidth = int(int(re.sub("[^0-9]","", borderwidth)) * px_to_dp_factor)
            borderwidth = 1 if (borderwidth < 1) else borderwidth

            heightAttribute = (borderwidth+1) if (shape_type == "line") \
                else (int(int(re.sub("[^0-9]","", height)) * px_to_dp_factor))

            f.write("""
    <size
        android:width="%sdp"
        android:height="%ddp"
    />\n""" % (
        int(int(re.sub("[^0-9]","", width)) * px_to_dp_factor),heightAttribute))

            if fill:
                f.write('   <solid\n       android:color="%s"\n    />\n' % \
                    ("@color/" + self.color_manager.find_or_create_id(fill)))

            if radius:
                f.write('   <corners\n       android:radius="%sdp"\n    />\n' % \
                    int(int(re.sub("[^0-9]","", radius)) * px_to_dp_factor))

            if bordercolor:
                f.write('   <stroke\n       android:color="%s"\n       android:width="%ddp"\n    />\n' % \
                    (("@color/" + self.color_manager.find_or_create_id(bordercolor)),\
                    borderwidth))

            f.write("</shape>")
        return name


    def add_image_layout(self,screen,tag,drawable_override=None):
        attributes = {}
        attributes["android:id"] = "@+id/" + self.__get_unique_id(tag["name"])

        drawable_name = drawable_override if (drawable_override is not None) else "ic_" +\
            utils.androidify_name((tag["name"]+"_"+str(tag['id'])))

        attributes["android:src"] = "@drawable/" + drawable_name

        attributes["android:scaleType"] = "center"
        attributes["zeroheight:zh_id"] = tag["id"]

        self.__handle_layout_attributes(screen,tag,attributes)

        if self.__is_full_width(screen,tag):
            attributes["android:scaleType"]     = "centerCrop"

        self.layout_details [tag['id']] = self.__generate_xml("ImageView",attributes)
        self.layout_ordering[tag['id']] = tag['z']

    def add_button_layout(self,screen,tag,bgShape=None):
        components = tag["components"]

        styles = None

        if "label" in components:
            styles = json.loads(components['label']['styles'].replace("=>",":"))

        px_to_dp_factor = self.__get_px_to_dp_factor(screen)
        attributes = {}
        attributes["android:id"] = "@+id/" + self.__get_unique_id(tag["name"])
        attributes["zeroheight:zh_id"] = tag["id"]

        bgDrawable = bgShape if (bgShape is not None) else ("ic_"+\
            utils.androidify_name(tag['name']+"_background"+"_"+str(components["background"]["id"])))

        if "subimage" in components:
            subimage_name = utils.androidify_name(tag['name']+"_subimage"+"_"+\
                str(components["subimage"]["id"]))
            attributes["android:drawableRight"] = "@drawable/ic_" + subimage_name

        attributes["android:background"] = "@drawable/" + bgDrawable
        tag['x'] = components['background']['x']
        tag['y'] = components['background']['y']
        tag['height'] = components['background']['height']
        tag['width'] = components['background']['width']

        if styles:
            attributes["android:textSize"] = "%dsp" % (int(float(re.sub("[^0-9.]", "",styles["fontSize"]))) * (px_to_dp_factor) * 1.1)
            attributes["android:textColor"] = "@color/" + self.color_manager.find_or_create_id(styles['color'])
            attributes["android:text"] = "@string/" + self.string_manager.find_or_create_id(styles['text'])

        self.__handle_layout_attributes(screen,tag,attributes)

        self.layout_details[tag['id']] = self.__generate_xml("Button",attributes)
        self.layout_ordering[tag['id']] = tag['z']

    def add_text_layout(self,screen,tag):
        px_to_dp_factor = self.__get_px_to_dp_factor(screen)

        styles = json.loads(tag['styles'].replace("=>",":"))

        attributes = {}
        attributes["android:id"] = "@+id/" + self.__get_unique_id(tag["name"])
        attributes["zeroheight:zh_id"] = tag["id"]

        if styles['alignment'] == "left" or \
            styles['alignment'] == "right" or \
            styles['alignment'] == "center":
            attributes["android:gravity"] = styles['alignment']
        else:
            attributes["android:gravity"] = "left"

        attributes["android:textSize"] = "%dsp" % (int(float(re.sub("[^0-9.]", "",styles["fontSize"]))) * (px_to_dp_factor))
        attributes["android:textColor"] = "@color/" + self.color_manager.find_or_create_id(styles['color'])
        attributes["android:text"] = "@string/" + self.string_manager.find_or_create_id(styles['text'])

        self.__handle_layout_attributes(screen,tag,attributes)

        attributes["android:layout_width"]  = "%ddp" % (tag['width']  * px_to_dp_factor)
        attributes["android:layout_height"] = "wrap_content"

        self.layout_details[tag['id']] = self.__generate_xml("TextView",attributes)
        self.layout_ordering[tag['id']] = tag['z']

    def begin_pull(self):
        self.color_manager.read_xml(self.asset_folder)
        self.string_manager.read_xml(self.asset_folder)

        full_dir = os.path.join(self.asset_folder, "values")
        if not os.path.exists(full_dir):
            utils.mkdir_p(full_dir)
        path = os.path.join(full_dir, "attrs.xml")

        with open(path, 'w') as f:
            f.write("""<?xml version="1.0" encoding="utf-8"?>
<resources>
    <attr name="zh_id" format="integer"/>
</resources>""")

    def finalise_pull(self):
        self.color_manager.write_xml(self.asset_folder)
        self.string_manager.write_xml(self.asset_folder)

    def finalise_screen(self,screen):
        full_dir = os.path.join(self.asset_folder, "layout")

        if not os.path.exists(full_dir):
            utils.mkdir_p(full_dir)

        android_screen_name = utils.androidify_name(screen['name'])
        path = os.path.join(full_dir, android_screen_name + ".xml")   

        print utils.COLOR_CODES.OKGREEN +"    Writing template layout xml to " + path + utils.COLOR_CODES.ENDC
        
        with open(path, 'w') as f:
            f.write(("""<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:zeroheight="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:id="@+id/%s"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:layout_gravity="top|center_horizontal"
    android:orientation="vertical"
    android:background="#ffffff"
    tools:context=".MainActivity" >\n""" % 
    self.__get_unique_id(android_screen_name).encode('utf-8')))

            ordered_layouts = sorted(self.layout_ordering.items(), 
                key=operator.itemgetter(1), 
                reverse=False)

            for pair in ordered_layouts:
                tag_id = pair[0]
                if tag_id in self.layout_details:
                    xml = self.layout_details[tag_id]
                    f.write(xml)

            f.write(("""
</RelativeLayout>""").encode('utf-8'))

    def should_fetch_resolution(self,resolution):
        return (resolution == "1" or resolution == "1.5" or resolution == "2" \
            or resolution == "3" or resolution == "4")

    def __handle_layout_attributes(self,screen,tag,attributes):

        px_to_dp_factor = self.__get_px_to_dp_factor(screen)

        attributes["android:layout_height"] = "wrap_content"

        if self.__is_full_width(screen,tag):
            attributes["android:layout_width"]  = "fill_parent"            
        else:
            attributes["android:layout_width"]  = "wrap_content"

        if self.__is_centralised(screen,tag):
            attributes["android:layout_centerHorizontal"] = "true"
        else:
            # if self.__is_left_of_center(screen,tag):
            attributes["android:layout_marginLeft"] = "%ddp" % (tag['x']  * px_to_dp_factor)
            # else:
            #     attributes["android:layout_marginRight"] = "%ddp" % \
            #         ((screen['width'] - tag['x'] - tag['width'])  * px_to_dp_factor)
            #     attributes["android:layout_alignParentRight"] = "true"

        # if self.__is_bottom_of_screen(screen,tag):
        #     attributes["android:layout_alignParentBottom"]="true"
        # else:
            # if self.__is_above_center(screen,tag):
        attributes["android:layout_marginTop"]  = "%ddp" % (tag['y']  * px_to_dp_factor)
            # else:
            #     attributes["android:layout_marginBottom"]  = "%ddp" % \
            #         ((screen['height'] - tag['y'] - tag['height'])  * px_to_dp_factor)
            #     attributes["android:layout_alignParentBottom"] = "true"

    def __get_unique_id(self,name):
        uid = a_name = utils.androidify_name(name)
        suffix = 1
        while uid in self.previous_ids:
            uid = a_name + "_" + str(suffix)
            suffix = suffix + 1

        self.previous_ids.add(uid)
        
        return uid

    def __is_full_width(self,screen,tag):
        return (tag['x'] == 0 and tag['width'] == screen['width'])

    def __is_centralised(self,screen,tag):
        right = (screen['width'] - (tag['width'] + tag['x']))
        diff_right_left = math.fabs(tag['x'] - right)
        return (diff_right_left >= 0 and diff_right_left < 5)

    def __is_left_of_center(self,screen,tag):
        return (tag['x'] < (screen['width'] / 2))

    def __is_above_center(self,screen,tag):
        return (tag['y']  < (screen['height'] / 2))

    def __is_bottom_of_screen(self,screen,tag):
        return((tag['y'] + tag['height']) == screen['height'])

    def __generate_xml(self,tag,attributes):
        xml = ("""
        <%s""" % tag)

        for attribute,value in attributes.iteritems():
            xml += ("\n            %s=\"%s\"" % (attribute,value))

        xml += "/>\n"

        return xml

    def __get_px_to_dp_factor(self,screen):
        designed_at = str(screen['designed_at'])
        dpi = self.__designed_at_to_dpi(designed_at)
        return (1 / (dpi / 160.0))

    def __designed_at_to_dpi(self,designed_at):
        if designed_at == "0.75":
            return 120.0
        if designed_at == "1":
            return 160.0
        if designed_at == "1.5":
            return 240.0
        if designed_at == "2":
            return 320.0
        if designed_at == "3":
            return 480.0
        if designed_at == "4":
            return 640.0
        return 160.0
